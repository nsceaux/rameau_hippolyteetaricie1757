\livretAct\line { ACTE CINQUIÉME }
\livretDescAtt\wordwrap-center {
  Le Théâtre représente un Jardin délicieux, qui forme les
  avenuës de la Forêt d'Aricie : On y voit \smallCaps Aricie,
  couchée sur un lit de verdure.
}
\livretScene\line { SCENE PREMIERE }
\livretPers Aricie
\livretRef #'EAAaricie
%# Ou suis-je ? de mes sens j'ai recouvré l'usage ;
%# Dieux, ne me l'avez-vous rendu,
%# Que pour me retracer l'image
%# Du tendre Amant que j'ai perdu ?

\livretRef #'EABaricie
\livretDidasPPage\line { La clarté se redouble. }
%# Quel doux Concerts ! Quel nouveau jour m'éclaire !
%# Non, non ; ces sons harmoni=eux,
%# Ce Soleil qui brille à mes yeux,
%# Sans Hippolyte, helas ! Rien ne me sçauroit plaire.
%# Mes Yeux, vous n'êtes plus ouverts,
%# Que pour verser des larmes.
%# En vain d'aimables sons font retentir les Airs ;
%# Je n'ai que des soupirs, pour répondre aux Concerts,
%# Dont ces lieux enchantés viennent m'offrir les charmes.
%# Mes Yeux, vous n'êtes plus ouverts,
%# Que pour verser des larmes.
\livretDidasP\line { Diane descend dans une gloire. }

\livretScene\line { SCENE II }
\livretDescAtt\wordwrap-center {
  \smallCaps { Diane, Aricie, } bergers & bergeres.
}
\livretPers Chœur
\livretRef #'EBAchoeur
%# Descendez, brillante Immortelle ;
%# Regnez à jamais dans nos bois.

\livretPers Aricie
\livretRef #'EBBaricieChoeur
%# Ciel ! Di=ane ! Malgré ma disgrace cru=elle,
%# Signalons l'ardeur de mon zèle
%# Pour la Divinité qui me tient sous ses Loix.
\livretPers Chœur
%#8 Descendez, etc.
\livretPers Aricie
%# Joignons nous aux voix
%# De cette Troupe fidelle.
%# Descendez, brillante Immortelle.
\livretPers Chœur
%#8 Regnez, etc.

\livretPers Diane
\livretRef #'EBCdiane
%# Peuples toûjours soûmis à mon obé=ïssance,
%# Que j'aime à me voir parmi vous !
%# Je fais mes plaisirs les plus doux
%# De regner sur des cœurs où regne l'innocence.
%# Pour dispenser mes Loix dans cet heureux séjour,
%# J'ai fait choix d'un Heros qui me chérit, que j'aime ;
%# Célébrez cet auguste jour ;
%# Que pour ce nouveau Maître, ainsi que pour moi-même,
%# Les plus beaux jeux soient préparez.
%# Allez-en prendre soin. Vous, Nymphe, demeurez.

\livretScene\line { SCENE III }
\livretDescAtt\wordwrap-center\smallCaps { Diane, Aricie. }
\livretPers Diane
\livretRef #'ECAdiane
%# Et vous : Troupe à ma voix fidelle,
%# Doux Zephirs, volez en ces lieux ;
%# Il est temps d'apporter le dépôt préci=eux
%# Que j'ai commis à vôtre zéle.
\livretRef #'EDAritournelle
\livretDidasPPage\justify {
  Les \smallCaps Zephirs amenent \smallCaps Hippolyte dans un Char.
}

\livretScene\line { SCENE IV }
\livretDescAtt\wordwrap-center\smallCaps { Diane, Hippolyte, Aricie. }
\livretPers Hippolyte et Aricie
\livretRef #'EDBaricieHippolyte
%# Aricie, =est-ce vous que je voi.
%# Hippolyte, est-ce vous que je voi.
%# Que mon sort est digne d'envie !
%# Le moment qui vous rend à moi,
%# Est le plus heureux de ma vie.

\livretPers Diane
\livretRef #'EDCdiane
%# Tendres Amans, vos malheurs sont finis ;
%# Pour votre *Hymen tout se prépare :
%# Ne craignez plus qu'on vous sépare,
%# C'est moi qui vous unis.
\livretDidasP\line { Bruits de musettes }
\livretPers Diane
%# Les Habitans de ces retraites
%# Ont préparé pour vous les plus aimables jeux ;
%# Et déja leurs douces Musettes
%# Annoncent le moment heureux,
%# Où vous allez regner sur eux.

\livretScene\line { SCENE V }
\livretDescAtt\wordwrap-center {
  \smallCaps { Diane, Hippolyte, }habitants de la Forêt d'Aricie.
}
\livretPers Chœur
\livretRef #'EEAmarcheChoeur
%# Chantons sur la Musette,
%# Chantons.
%# Au son qu'elle répette,
%# Dansons.
%# Que l'Echo fidèle
%# Rende nos chansons.
%#6 Chantons, etc.
%# Bergère trop cru=elle,
%# Goûtez les tendres leçons.
%#6 Chantons sur la Musette, etc.

\livretRef #'EEBrondeau
\livretDidasPPage\line { On danse. }

\livretPers Une Bergère
\livretRef #'EECbergere
%# Plaisirs, doux Vainqueurs,
%# A qui tout rend les Armes,
%# Enchaînez les cœurs ;
%# Plaisirs, doux Vainqueurs,
%# Rassemblez tous vos charmes ;
%# Enchantez tous les cœurs.
%# Que l'Amour a d'appas ;
%# Regnez, ne cessez pas
%# De voler sur ces pas.
%#5 Plaisirs, doux Vainqueurs, etc.
%# C'est aux Ris, c'est au Jeux
%# D'embellir son Empire ;
%# Qu'aussi-tôt qu'on soupire,
%# L'on y soit heureux.
%#5 Plaisirs, doux Vainqueurs, etc.

\livretRef #'EEDmenuet
\livretDidasPPage\line { On danse. }

\livretPers Diane
\livretRef #'EEFdiane
%# Bergers, vous allez voir combien je suis fidèle
%# A tenir ce que je promets ;
%# Le Heros, qui sur vous va regner desormais,
%# Sera le prix de votre zèle.
\livretPers Chœur
%#8 [Que tout soit heureux sous les Loix
%# Du Roi que Di=ane nous donne ;
%# Que tout applaudisse à son choix ;
%#8 C'est la Vertu qui le couronne.]

\livretRef #'EEGgavotte
\livretDidasPPage\line { On danse. }

\livretPers Aricie
\livretRef #'EEIariette
%# Rossignols amoureux, répondez à nos voix ;
%# Par la douceur de vos ramages,
%# Rendez les plus tendres hommages
%# A la Divinité qui regne dans nos Bois.
\null
\livretRef #'EEJchaconne
\livretDidasPPage\justify { Un ballet général termine le Divertissement. }
