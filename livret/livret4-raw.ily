\livretAct\line { ACTE QUATRIÉME }
\livretDescAtt\wordwrap-center {
  Le Théâtre représente un Bois consacré à \smallCaps Diane
  sur le rivage de la Mer.
}
\livretScene\line { SCENE PREMIERE }
\livretPers Hippolyte
\livretRef #'DAAhippolyte
%# Ah ! Faut-il en un jour, perdre tout ce que j'aime !
%# Mon Pere pour jamais me bannit de ces lieux ;
%# Si cheris de Di=ane même,
%# Je ne verrai plus les beaux yeux
%# Qui faisoient mon bonheur suprême :
%# Ah ! Faut-il en un jour, perdre tout ce que j'aime !

\livretScene\line { SCENE II }
\livretDescAtt\wordwrap-center\smallCaps { Hippolyte, Aricie. }
\livretPers Aricie
\livretRef #'DBAhippolyteAricie
%# C'en est donc fait, cru=el, rien n'arrête vos pas,
%# Vous desesperez votre amante.
\livretPers Hippolyte
%# Helas ! Plus je vous vois, plus ma douleur augmente,
%# Je sens mieux tous mes maux quand je vois tant d'appas.
\livretPers Aricie
%# Quoi ! L'inimitié de la Reine,
%# Vous fait-elle quitter l'objet de votre amour ?
\livretPers Hippolyte
%# Non ! Je ne fuirois pas de cet heureux séjour
%# Si je n'y craignois que sa haine.
\livretPers Aricie
%#- Que dites-vous...
\livretPers Hippolyte
%#= Gardez d'oser porter les yeux
%# Sur le plus horrible mystere,
%# Le respect me force à me taire ;
%# J'offenserois le Roi, Di=ane, & tous les Dieux.
\livretPers Aricie
%# Ah ; c'est m'en dire assez, ô crime !
%# Mon cœur en est glacé d'épouvante & d'horreur.
%# Cependant vous partez, & de Phedre en fureur
%# Je vais devenir la victime.
\livretDidasPPage à part.
%# Dieux ; pourquoi séparer deux cœurs
%# Que l'amour a faits l'un pour l'autre !
\livretDidasPPage à Hippolyte.
%# Eh ! Quelle autre main que la vôtre,
%# Si vous m'abandonnez, pour essuy=er mes pleurs ?
\livretDidasPPage à part.
%# Dieux ; pourquoi séparer deux cœurs
%# Que l'amour a faits l'un pour l'autre ?
\livretPers Hippolyte
%#- Hé bien daignez me suivre.
\livretPers Aricie
%#= O ciel ! Que dites-vous ?
%#- Moi vous suivre !
\livretPers Hippolyte
%#= Cessez de croire
%# Que je puisse oubli=er le soin de votre gloire.
%# En suivant votre amant, vous suivez votre époux ;
%# Venez... quel silence funeste !
\livretPers Aricie
%# Ah ! Prince, croyez-en l'amour que j'en atteste,
%# Je ferois mon suprême bien
%# D'unir votre sort & le mien ;
%# Mais Di=ane est inéxorable
%# Pour l'amour & pour les Amans.
\livretPers Hippolyte
%# A d'innocens désirs Diane est favorable
%# Qu'elle préside à nos sermens.
\livretPers Ensemble
%# Nous allons nous jurer une immortelle foi :
%# Viens, Reine des Forêts, viens former notre chaîne ;
%# Que l'encens de nos vœux s'éleve jusqu'à toi,
%# Sois toujours de nos cœurs l'unique Souveraine.

\livretRef #'DBBannonce
\livretDidasPPage\line { On entend un bruit de Cors. }

\livretPers Hippolyte
\livretRef #'DBChippolyte
%# Le sort conduit ici ses sujets fortunés ;
%# Unissons-nous aux jeux qui lui sont destinés.

\livretScene\line { SCENE III }
\livretDescAtt\wordwrap-center {
  \smallCaps { Hippolyte, Aricie, } chasseurs et chasseresses.
}
\livretPers Chœur
\livretRef #'DCAchoeur
%# Faisons par tout voler nos traits.
%# Animons-nous à la victoire ;
%# Que les antres les plus secrets
%# Retentissent de notre gloire.

\livretRef #'DCBentree
\livretDidasPPage\line { On danse. }

\livretPers Une Chasseresse
\livretRef #'DCDchasseresse
%# Amans, quelle est votre foiblesse ?
%# Vo=yez ! L'Amour sans vous allarmer ;
%# Ces mêmes traits dont il vous blesse,
%# Contre nos cœurs n'osent plus s'armer.
%# Malgré ses charmes
%# Les plus doux,
%# Bravez ses armes,
%# Faites comme nous ;
%# Osez, sans allarmes,
%# Attendre ses coups ;
%# Si vous combattez, la victoire est à vous,
%# Amans, quelle est votre foiblesse ?
%# Voy=ez ! L'Amour sans vous allarmer ;
%# Ces mêmes traits dont il vous blesse,
%# Contre nos cœurs n'osent plus s'armer.
%# Vous vous plaignez qu'il a des rigueurs,
%# Et vous aimez tous les traits qu'il vous lance !
%# C'est vous qui les rendez vainqueurs ;
%# Pourquoi sans défense
%# Livrer vos cœurs ?
%#8 Amans, quelle est votre foiblesse, &c.

\livretRef #'DCEmenuet
\livretDidasPPage\line { On danse. }

\livretPers Une Chasseresse
\livretRef #'DCGrondeauChoeur
%# A la chasse, à la chasse,
%# Armez-vous.
\livretPers Chœur
%# Courons tous à la chasse ;
%# Armons-nous.
\livretPers Une Chasseresse
%# Dieu des cœurs, cédez la place ;
%# Non, non, ne regnez jamais.
%# Que Di=ane préside ;
%# Que Di=ane nous guide,
%# Dans le fond des forêts ;
%# Sous ses loix nous vivons en paix.
%#6 A la chasse, &c.
\livretPers Une Chasseresse
%# Nos asyles
%# Sont tranquilles,
%# Non, non, rien n'a plus d'attraits.
%# Les plaisirs sont parfaits,
%# Auncun soin n'embarrasse,
%# On y rit des Amours,
%# On y passe les plus beaux jours.
%# A la chasse, &c.
\livretDidasPPage\line { On danse. }

\livretRef #'DCHchoeurHippolyteAricie
\livretDidasPPage\justify { La mer s'agite ; on en voit sortir un monstre horrible. }
\livretPers Chœur
%# Quel bruit ! Quels vents ! Quelle montagne *humide !
%# Quel monstre elle enfante à nos yeux ?
%# O Di=ane, accourez ; volez du haut des cieux.
\livretPersDidas Hippolyte s’avance vers le monstre.
%# Venez, qu’à son défaut je vous serve de guide.
\livretPers Aricie
%# Arrête.

\livretPers Chœur
\livretRef #'DCIchoeurAricie
%# Dieux ! Quelle flamme l'environne !
\livretPers Aricie
%# Quel nu=age épais ! Tout se dissipe ; hélas ?
%# Hippolyte ne paroît pas.
%# Je meurs.
\livretDidasP\line { \smallCaps Aricie tombe évanouie. }
\livretPers Chœur
%# O disgrace cru=elle !
%# Hippolyte n'est plus.

\livretScene\line { SCENE IV }
\livretDescAtt\wordwrap-center {
  \smallCaps Phedre, chasseurs et chasseresses.
}
\livretPers Phedre
\livretRef #'DDAphedreChoeur
%# Quelle Plainte en ces lieux m'appelle !
\livretPers Chœur
%# Hippolyte n'est plus.
\livretPers Phedre
%# Il n'est plus ! O douleur mortelle !
\livretPers Chœur
%# O regrets superflus !
\livretPers Phedre
%# Quel sort l'a fait tomber dans la nuit éternelle !
\livretPers Chœur
%# Un Monstre furi=eux sorti du sein des flots,
%# Vient de vous ravir ce Héros.
\livretPers Phedre
%# Non, sa mort est mon seul ouvrage ;
%# Dans les Enfers, c'est par moi qu'il descend ;
%# Neptune de Thesée =a crû venger l'outrage ;
%# J'ai versé le sang innocent.
%# Qu'ai-je fait ? quels remords ! Ciel ! J'entens le tonnerre.
%# Quel bruit ! Quels terribles éclats ?
%# Fuy=ons ; où me cacher ? je sens trembler la terre ;
%# Les Enfers s'ouvrent sous mes pas.
%# Tous les Dieux conjurez, pour me livrer la guerre,
%# Arment leurs redoutables bras.
%# Dieux cru=els, Vengeurs implacables,
%# Suspendez un courroux qui me glace d'effroi ;
%# Ah ! Si vous êtes équitables,
%# Ne tonnez pas encor sur moi ;
%# La gloire d'un Héros que l'imposture opprime ;
%# Vous demande un juste secours ;
%# Laissez-moi révéler à l'Auteur de ses jours,
%# Et son innocence & mon crime.
\livretPers Chœur
%# O remords superflus !
%# Hippolyte n'est plus.
\sep
