\livretAct\line { ACTE TROISIÉME }
\livretDescAtt\wordwrap-center {
  Le Théâtre représente une partie du palais de \smallCaps Thesée,
  sur le rivage de la mer.
}
\livretScene\line { SCENE PREMIERE }
\livretPers Phedre
\livretRef #'CAAphedre
%# Cru=elle Mere des Amours,
%# Ta vengeance a perdu ma trop coupable race,
%# N'en suspendras-tu point le cours ?
%# Ah ! Du moins, à tes yeux, que Phedre trouve grace.
%#8 [Je ne te reproche plus rien,
%# Si tu rends à mes vœux Hippolyte sensible ;
%# Mes feux me font horreur, mais mon crime est le tien ;
%# Tu dois cesser d'être inflexible.
%#8 Cru=elle Mere des Amours, etc.]
\livretRef #'CABphedre
%# Mais pourquoi tous ces vains remords !
%# Ah ! Si j'en crois Arcas, mon cœur peut tout prétendre,
%# Thésée =a vû les sombres bords.
%# L'Enfer, pour me punir, pourroit-il me le rendre !…

\livretScene\line { SCENE II }
\livretDescAtt\wordwrap-center\smallCaps { Phedre, Hippolyte, Œnone. }
\livretPers Hippolyte
\livretRef #'CBAphedreHippolyte
%# Reine, sans l'ordre exprès, qui dans ces lieux m'appelle,
%# Quand le ciel vous ravit un époux glori=eux,
%# Je respecterois trop votre douleur mortelle,
%# Pour vous montrer encore un objet odi=eux.
\livretPers Phedre
%# Vous, l'objet de ma haine ! O ciel ! Quelle injustice !
%# Je dois dissiper cette erreur ;
%# Helas ! Si vous croy=ez que Phedre vous ha=ïsse,
%# Que vous connoissez mal son cœur !
\livretPers Hippolyte
%# Qu'entens-je ? A mes desirs Phedre n'est plus contraire !
%# Ah ! Les plus tendres soins de votre auguste époux
%# Dans mon cœur désormais vont revivre pour vous.
\livretPers Phedre
%#- Quoi ? Prince...
\livretPers Hippolyte
%#= A votre fils je tiendrai lieu de Perre ;
%# J'affermirai son trône, & j'en donne ma foi.
\livretPers Phedre
%# Vous pourriez jusques-là vous attendrir pour moi !
%# C'en est trop ; & le trône, & le fils, & la mere,
%# Je range tout sous votre loi.
\livretPers Hippolyte
%# Non ; dans l'art de regner je l'instruirai moi-même ;
%# Je céde sans regret la suprême grandeur.
%# Aricie =est tout ce que j'aime ;
%# Et si je veux regner, ce n'est que dans sons cœur.
\livretPers Phedre
%# Que dites-vous ? O Ciel ! Quelle étoit mon erreur !
%# Malgré mon trône offert, vous aimez Aricie !
\livretPers Hippolyte
%# Quoi ! Votre haine encor n'est donc pas adoucie ?
\livretPers Phedre
%# Tu viens d'en redoubler l'horreur...
%# Puis-je trop ha=ïr ma rivale ?
\livretPers Hippolyte
%# Votre rivale ! Je fremis ;
%# Thésée =est votre époux, & vous aimez son fils !
%# Ah ! Je me sens glacer d'une horreur sans égale.
%# Terribles ennemis des perfides humains,
%# Dieux, si prompts autrefois à les réduire en poudre,
%# Qu'attendez-vous ? Lancez la foudre.
%# Qui la retient entre vos mains ?
\livretPers Phedre
%# Ah ! Cesse par tes vœux d'allumer le tonnerre.
%# Eclatte ; éveille-toi ; sors d'un honteux repos ;
%# Rends-toi digne fils d'un heros,
%# Que de monstres sans nombre a délivré la terre ;
%# Il n'en est échappé qu'un seul à sa fureur ;
%# Frappe ; ce monstre est dans mon cœur.
\livretPers Hippolyte
%#- Grands Dieux !
\livretPers Phedre
%#= Tu balances encore !
%# Etouffe dans mon sang un amour que j'abhorre.
%# Je ne puis obtenir ce funeste secours !
%# Cruel ! Quelle rigueur extrême !
%# Tu me hais, autant que je t'aime ;
%# Mais, pour trancher mes tristes jours,
%# Je n'ai besoin que de moi-même.
%#- Donne...
\livretPers Hippolyte
%#- Que faites-vous ?
\livretPers Phedre
%#= Tu m'arraches ce fer,
\livretDidasP\line { \smallCaps Thesée paroît. }

\livretScene\line { SCENE III }
\livretDescAtt\wordwrap-center{ \smallCaps Thesée ; & les Acteurs de la scene précédente. }
\livretPers Thesée
\livretRef #'CCAtheseeHippolytePhedre
%# Que vois-je ? Quel affreux spectacle !
\livretPers Hippolyte
%#- Mon pere !
\livretPers Phedre
%#- Mon époux.
\livretPersDidas Thesée à part
%#= O trop fatal Oracle !
%# Je trouve les malheurs que ma prédits l'Enfer.
\livretDidasP\line { à Phedre. }
%# Reine, dévoilez-moi ce funeste mystére.
\livretPersDidas Phedre à Thesée
%# N'approchez point de moi ; l'Amour est outragé ;
%# Que l'Amour soit vengé.
\livretScene\line { SCENE IV }
\livretDescAtt\wordwrap-center\smallCaps { Thesée, Hippolyte, Œnone. }
\livretPersDidas Thesée à Hippolyte
\livretRef #'CDAtheseeHippolyte
%# Sur qui doit tomber ma colere ?
%# Parlez, mon fils, parlez, nommez le criminel.
\livretPers Hippolyte
%# Seigneur... Dieux ! Que vais-je lui dire ?
%# Permettez que je me retire ;
%# Ou plutôt, que j'obtienne un exil éternel.
\livretDidasP\line { \smallCaps Hippolyte sort. }

\livretScene\line { SCENE V }
\livretDescAtt\wordwrap-center\smallCaps { Thesée, Œnone. }
\livretPers Thesée
\livretRef #'CEAtheseeOenone
%# Quoi ? Tout me fuit ! Tout m'abandonne !
%# Mon épouse ! Mon fils ! Ciel ! demeurez, Œnone.
%# C'est à vous seule à m'éclairer
%# Sur la trahison la plus noire.
\livretPers Œnone
%# Ah ! Sauvons de la reine & les jours & la gloire.
%# Un desespoir affreux... pouvez-vous l'ignorer ?
%# Vous n'en avez été qu'un témoin trop fidéle.
%# Je n'ose accuser votre fils ;
%# Mais, la reine... Seigneur, ce fer armé contre elle,
%# Ne vous en a que trop appris.
\livretPers Thesée
%#- Dieux ! Acheve.
\livretPers Œnone
%#= Un amour funeste...
\livretPers Thesée
%# C'en est assez ; épargne-moi le reste.

\livretScene\line { SCENE VI }
\livretPers Thesée
\livretRef #'CFAthesee
%# Qu'ai-je appris ? Tous mes sens en sont glacez d'horreur.
%# Vengeons-nous ; quel projet ! Je fremis quand j'y pense.
%# Qu'il en va coûter à mon cœur !
%# A punir un ingrat d'où vient que je balance ?
%# Quoi ? Ce sang, qu'il trahit, me parle en sa faveur !
%# Non, non, dans un fils si coupable,
%# Je ne vois qu'un monstre effroy=able :
%# Qu'il ne trouve en moi qu'un vengeur.
\null
\livretRef #'CFBthesee
%# Puissant maître des flots, favorable Neptune,
%# Entens ma gémissante voix ;
%# Permets que ton fils t'importune,
%# Pour la derniere fois.
%# Hippolyte m'a fait le plus sanglant outrage ;
%# Rempli le serment qui t'engage ;
%# Préviens par son trépas un desespoir affreux ;
%# Ah ! Si tu refusois de venger mon injure,
%# Je serois parricide, & tu serois parjure;
%# Nous serions coupables tous deux.
\livretRef #'CFCthesee
\livretDidasPPage\line { La mer s'agite. }
%# Mais de courroux l'onde s'agite.
%# Tremble ; tu vas périr, trop coupable Hippolyte.
%# Le sang a beau cri=er, je n'entens plus sa voix.
%# Tout s'apprête à punir une offense mortelle ;
%# Neptune me sera fidéle,
%# C'est aux Dieux à venger les Rois.
\null
\livretRef #'CFEthesee
%# On vient de mon retour rendre grace à Neptune,
%# Et je voudrois encore être dans les Enfers :
%# Fuy=ons une foule importune ;
%# Ne puis-je disparoître aux yeux de l'univers !

\livretScene\line { SCENE VII }
\livretDescAtt\wordwrap-center\smallCaps { Thesée, peuples et Matelots. }
\livretPers Chœur
\livretRef #'CGBchoeur
%# Que ce rivage retentisse
%# De la gloire du Dieu des flots :
%# Qu'à ses bienfaits tout applaudisse ;
%# Il rend à l'univers le plus grand des heros.
%# Que ce rivage retentisse
%# De la gloire du Dieu des flots.
\livretRef #'CGCair
\livretDidasPPage\line { On danse. }
\livretPers Une Matelote
\livretRef #'CGGmatelote
%# L'Amour, comme Neptune,
%# Invite =à s'embarquer ;
%# Pour tenter la fortune,
%#6 On ose tout risquer.
%# Malgré tant de naufrages,
%# Tous les cœurs sont matelots ;
%# On quitte le repos ;
%# On vole sur les flots ;
%# On affronte les orages ;
%# L'Amour ne dort
%# Que dans le Port.
\livretDidasP\line { On danse. }
\sep
