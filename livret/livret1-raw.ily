\livretAct\line { ACTE PREMIER }
\livretDescAtt\center-column {
  \wordwrap-center { Le Théâtre représente un temple consacré à \smallCaps Diane : }
  \line { On y voit un autel. }
}
\livretScene\line { SCENE PREMIERE }
\livretPersDidas Aricie en Chasseresse.
\livretRef #'AABaricie
%# Temple sacré, séjour tranquille,
%# Où Di=ane aujourd'hui doit recevoir mes vœux,
%# A mon cœur agité daigne servir d'asyle
%# Contre un amour trop malheureux.
%# Et toi, dont malgré-moi je rappelle l'image,
%# Cher Prince, si mes vœux ne te sont pas offerts,
%# Du moins, j'en apporte l'hommage
%# A la Dé=esse que tu sers.
%# Temple sacré, séjour tranquille,
%# Où Di=ane aujourd'hui doit recevoir mes vœux,
%# A mon cœur agité daigne servir d'asyle
%# Contre un amour trop malheureux.

\livretScene\line { SCENE II }
\livretDescAtt\wordwrap-center\smallCaps { Hippolyte, Aricie. }
\livretPers Hippolyte
\livretRef #'ABAhippolyteAricie
%# Princesse, quels apprêts me frappent dans ce Temple !
\livretPers Aricie
%# Diane préside en ces lieux ;
%# Lui consacrer mes jours, c'est suivre votre exemple.
\livretPers Hippolyte
%# Non, vous les immolez, ces jours si préci=eux.
\livretPers Aricie
%# J'exécute du Roi la volonté suprême ;
%# A Thésée, à son Fils, ces jours sont odieux.
\livretPers Hippolyte
%# Moi, vous ha=ïr ! O Ciel ! Quelle injustice extrême !
\livretPers Aricie
%# Je ne suis point l'objet de votre inimitié ?
\livretPers Hippolyte
%# Je sens pour vous une pitié
%# Aussi tendre que l'amour même.
\livretPers Aricie
%#- Quoi ? Le fier *Hippolyte…
\livretPers Hippolyte
%#= *Hélas !
%# Je n'en ai que trop dit ; je ne m'en repens pas,
%# Si vous avez daigné m'entendre :
%# Mon trouble, mes soûpirs, vos malheurs, vos appas,
%# Tout vous annonce un cœur trop sensible & trop tendre.
\livretPers Aricie
%# Ah ! Que venez-vous de m'apprendre !
%# C'en est fait ; pour jamais mon repos est perdu.
%# Peut-être votre indifférence
%# Tôt ou tard me l'auroit rendu ;
%# Mais votre amour m'en ôte l'esperance.
%# C'en est fait ; pour jamais mon repos est perdu.
\livretPers Hippolyte
%# Qu'entends-je ! Quel transport de mon ame s'empare !
\livretPers Aricie
%# Oubliez-vous qu'on nous sépare !
%# Quel temple redoutable, & quel affreux li=en !

\livretRef #'ABBaricie
%# Hippolyte amoureux m'occupera sans cesse ;
%# Même aux Autels de la Dé=esse,
%# Je sentirai mon cœur s'élancer vers le sien.
%# Di=ane & l'univers pour moi ne sont plus rien.
%# Hippolyte amoureux m'occupera sans cesse,
%# Je vivrai pour pleurer son malheur & le mien.

\livretPers Hippolyte
\livretRef #'ABChippolyteAricie
%# Je vous affranchirai d'une loi si cru=elle.
\livretPers Aricie
%# Phédre sur sa captive à des droits absolus ;
%# Que sert de nous aimer ? Nous ne nous verrons plus.
\livretPers Hippolyte
%# O Di=ane ! Protége une flamme si belle.

\livretPers Aricie et Hippolyte
\livretRef #'ABDduo
%# Nous brûlons des plus pures flammes,
%# L'Amour n'offre à nos cœurs que d'innocens appas,
%# Tu ne le défends pas,
%# Non, non, tu ne le défends pas
%# Quand c'est par la vertu qu'il regne sur nos ames.

\livretScene\line { SCENE III }
\livretDescAtt\wordwrap-center\smallCaps {
  Hippolyte, Aricie, La Grande Prêtresse de Diane, Prêtresses de Diane.
}
\livretRef #'ACAmarche
\livretDidasPage Entrée des Prêtresses
\livretPers Chœur
\livretRef #'ACBchoeur
%# Dans ce paisible séjour,
%# Regne l’aimable innocence :
%# Les traits que lance l’Amour
%# Sur nous n’ont point de puissance ;
%# Nous jouissons à jamais
%# des doux charmes de la paix.

\livretRef #'ACCair
\livretDidasPPage\line { On danse. }

\livretPers La Grande Prêtresse
\livretRef #'ACDpretresse
%# Dieu d'Amour, pour nos asyles,
%# Tes tourmens ne sont pas faits.
%# Tous les cœurs y sont tranquilles,
%# Tes efforts sont inutiles ;
%# Non, non, tu n'en peux troubler la paix.
%# Tes allarmes
%# Ont des charmes
%# Pour qui manque de raison
%# Mais nos ames
%# De tes flammes
%# Reconnoissent le poison :
%# Va, fuis ; pers l'esperance :
%# Va, fuis loin de nos cœurs :
%# Tu n'as point de traits vainqueurs.

\livretRef #'ACEair
\livretDidasPPage\line { On danse. }
\livretPersDidas La Grande Prêtresse alternativement avec le chœur
\livretRef #'ACFgavotte
%# De l'amour fuy=ez les charmes
%# Craignez jusqu'à ses douceurs,
%# De fleurs il couvre ses armes,
%# Mais les larmes,
%# Les allarmes,
%# Sont le prix des tendres cœurs.
\livretDidasP\line { On danse. }
\livretPersDidas La Grande Prêtresse et le Chœur alternativement avec le chœur
%# La paix & l'indifférence
%# Comblent ici nos désirs ;
%# Les biens que l'amour dispense
%# Coûtent toujours des soûpirs ;
%# Dans le sein de l'innocence
%# Nous trouvons les vrais plaisirs.
\livretDidasP\line { On danse. }

\livretScene\line { SCENE IV }
\livretDescAtt\wordwrap-center {
  \smallCaps { Phedre, Œnone, Gardes ; } et les Acteurs de la Scene
  précédente.
}
\livretPersDidas Phedre à Aricie
\livretRef #'ADAphedreAricieChoeurHippolyte
%# Princesse, ce grand jour par des nœuds éternels
%# Va vous unir aux Immortels.
\livretPers Aricie
%# Je crains que le ciel ne condamne
%# L'hommage que j'apporte aux pieds des saints autels.
%# Quel cœur viens-je offrir à Di=ane !
\livretPers Phedre
%#- Quel discours !
\livretPers Aricie
%#= Sans remors, comment puis-je en ces lieux,
%# Offrir un cœur que l'on opprime ?
\livretPers Chœur des Prêtresses
%# Non, non, un cœur forcé n'est pas digne des Dieux ;
%# Le sacrifice en est un crime.
\livretPers Phedre
%# Quoi ? L'on ose braver le suprême pouvoir !
\livretPers Chœur
%# Obé=ïssez au Dieux ; c'est le premier devoir.
\livretPersDidas Phedre à Hippolyte
%# Prince, vous souffrez qu'on outrage
%# Et votre Pere, & votre Roi !
\livretPersDidas Hippolyte à Phedre
%# Vous sçavez que respect à Di=ane m'engage ;
%# Dès mes plus tendres ans je lui donnai ma foi.
\livretPers Phedre
%# Dieux ! Thésée =en son fils trouve un sujet rebelle !
\livretPers Hippolyte
%# Je sais tout ce que je lui doi ;
%# Mais, ne puis-je pour lui faire éclatter mon zéle,
%# Qu'en outrageant une Immortelle ?
\livretPers Phedre
%# Laissez des détours superflus ;
%# La vertu quelquefois sert de prétexte au crime.
\livretPers Hippolyte
%#- Quel crime !
\livretPers Phedre
%#= Je ne sais qui vous touche le plus,
%# De l'autel, ou de la victime.
\livretPers Hippolyte
%# Du moins, par d'injustes rigueurs,
%# Je ne sais point forcer les cœurs.

\livretPers Phedre
\livretRef #'ADBphedre
%# Périsse la vaine puissance
%# Qui s'éleve contre les Rois :
%# Tremblez ; redoutez ma vengeance,
%# Et le Temple & l'Autel vont tomber à ma voix.
%# Tremblez, j'ai sû prévoir la désobé=ïssance ;
%# Périsse la vaine puissance,
%# Qui s'éleve contre les Rois.

\livretRef #'ADCprelude
\livretDidasPPage\line { Bruit de trompettes. }
\livretDidasPPage\line { Des Guerriers entrent, & vont briser l'Autel. }

\livretPers La Grande Prêtresse et le Chœur
\livretRef #'ADDpretresseChoeur
%# Dieux vengeurs, lancez le tonnerre :
%# Périssent les mortels qui vous livrent la guerre.

\livretRef #'ADEtonnerre
\livretDidasPPage\line { Bruit de tonnerre. }
\livretDidasPPage\line { \smallCaps Diane paroît dans une gloire. }
\livretPers La Grande Prêtresse
%# [Nos cris sont montés jusqu'aux cieux.
%# La Dé=esse descend ; tremblez, audaci=eux.]

\livretScene\line { SCENE V }
\livretDescAtt\wordwrap-center {
  \smallCaps Diane ; & les Acteurs de la Scene précédente.
}
\livretPersDidas Diane à ses Prêtresses
\livretRef #'AEAdianeAricieHippolyte
%# Ne vous allarmez pas d'un projet téméraire,
%# Tranquilles cœurs, qui vivez sous ma loi.
%# Vous voy=ez Jupiter se déclarer mon Pere ;
%# Sa foudre vole devant moi.
\livretDidasP\line { à Phedre }
%# Toi, tremble Reine sacrilege ;
%# Penses-tu m'honorer par d'unjustes rigueurs ?
%# Apprens que Dia=ne protége
%# La liberté des cœurs.
\livretDidasP\line { à Aricie }
%# Et toi, triste victime, à me suivre fidéle,
%# Fais toujours expirer les monstres sous tes traits.
%# On peut servir Di=ane avec le même zéle,
%# Dans son temple & dans les forêts.
\livretPers Hippolyte et Aricie
%#- Dé=esse, pardonnez...
\livretPers Diane
%#= Votre vertu m'est chere ;
%# Et c'est au crime seul que je dois ma colere.
\livretDidasP\justify {
  \smallCaps Diane entre dans son temple avec ses pretresses, 
  & \smallCaps Hippolyte emméne \smallCaps Aricie.
}

\livretScene\line { SCENE VI }
\livretDescAtt\wordwrap-center { \smallCaps Phedre. }
\livretPers Phedre
\livretRef #'AFAphedre
%# Quoi ! La terre & le ciel contre moi sont armés !
%# Ma rivale me brave ! Elle suit Hippolyte !
%# Ah ! Plus je vois leurs cœurs l'un pour l'autre enflamés,
%# Plus mon jaloux transport s'irrite.
\null
%# Que rien n'échappe à ma fureur ;
%# Immolons à la fois l'amant & la rivale :
%# Haine, dépit, rage infernale,
%# Je vous abandonne mon cœur.
\sep
