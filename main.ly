\version "2.19.83"
\include "common.ily"

\paper {
  %% de la place sous la basse pour pouvoir noter les chiffrages
  score-markup-spacing.padding = #4
  system-system-spacing.padding = #4
  last-bottom-spacing.padding = #4
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = "Hippolyte et Aricie"
    editions = #(and (eqv? #t (ly:get-option 'urtext))
                     (markup #:italic "urtext"))
  }
  \markup \null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \override-lines #'(column-number . 2) 
  \override-lines #'(use-rehearsal-numbers . #t)
  \table-of-contents
}

%% Notes
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "notes.ily"
}
%% Livret
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "livret/livret.ily"
}

\include "acte1.ily"
\include "acte2.ily"
\include "acte3.ily"
\include "acte4.ily"
\include "acte5.ily"
