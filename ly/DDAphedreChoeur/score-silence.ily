\markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Phedre
    \livretVerse#8 { Quelle Plainte en ces lieux m’appelle ! }
    \livretPers Chœur
    \livretVerse#6 { Hippolyte n’est plus. }
    \livretPers Phedre
    \livretVerse#8 { Il n’est plus ! O douleur mortelle ! }
    \livretPers Chœur
    \livretVerse#6 { O regrets superflus ! }
    \livretPers Phedre
    \livretVerse#12 { Quel sort l’a fait tomber dans la nuit éternelle ! }
    \livretPers Chœur
    \livretVerse#12 { Un Monstre furieux sorti du sein des flots, }
    \livretVerse#8 { Vient de vous ravir ce Héros. }
    \livretPers Phedre
    \livretVerse#8 { Non, sa mort est mon seul ouvrage ; }
    \livretVerse#10 { Dans les Enfers, c’est par moi qu’il descend ; }
    \livretVerse#12 { Neptune de Thesée a crû venger l’outrage ; }
    \livretVerse#8 { J’ai versé le sang innocent. }
  }
  \column {
    \livretVerse#12 { Qu’ai-je fait ? quels remords ! Ciel ! J’entens le tonnerre. }
    \livretVerse#8 { Quel bruit ! Quels terribles éclats ? }
    \livretVerse#12 { Fuyons ; où me cacher ? je sens trembler la terre ; }
    \livretVerse#8 { Les Enfers s’ouvrent sous mes pas. }
    \livretVerse#12 { Tous les Dieux conjurez, pour me livrer la guerre, }
    \livretVerse#8 { Arment leurs redoutables bras. }
    \livretVerse#8 { Dieux cruels, Vengeurs implacables, }
    \livretVerse#12 { Suspendez un courroux qui me glace d’effroi ; }
    \livretVerse#8 { Ah ! Si vous êtes équitables, }
    \livretVerse#8 { Ne tonnez pas encor sur moi ; }
    \livretVerse#12 { La gloire d’un Héros que l’imposture opprime ; }
    \livretVerse#8 { Vous demande un juste secours ; }
    \livretVerse#12 { Laissez-moi révéler à l’Auteur de ses jours, }
    \livretVerse#8 { Et son innocence & mon crime. }
    \livretPers Chœur
    \livretVerse#6 { O remords superflus ! }
    \livretVerse#6 { Hippolyte n’est plus. }
  }
}
