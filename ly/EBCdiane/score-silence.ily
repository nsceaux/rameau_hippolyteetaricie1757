\markup\column {
  \livretPers Diane
  \livretVerse#12 { Peuples toûjours soûmis à mon obéïssance, }
  \livretVerse#8 { Que j’aime à me voir parmi vous ! }
  \livretVerse#8 { Je fais mes plaisirs les plus doux }
  \livretVerse#12 { De regner sur des cœurs où regne l’innocence. }
  \livretVerse#12 { Pour dispenser mes Loix dans cet heureux séjour, }
  \livretVerse#12 { J’ai fait choix d’un Heros qui me chérit, que j’aime ; }
  \livretVerse#8 { Célébrez cet auguste jour ; }
  \livretVerse#12 { Que pour ce nouveau Maître, ainsi que pour moi-même, }
  \livretVerse#8 { Les plus beaux jeux soient préparez. }
  \livretVerse#12 { Allez-en prendre soin. Vous, Nymphe, demeurez. }
}
