\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:notes "violon-clavecin"
          #:score-template "score-basse-continue-voix"
          #:instrument "Clavecin"
          #:clef "treble")
   (silence #:score "score-silence"))
