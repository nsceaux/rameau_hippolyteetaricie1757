\markup\column {
  \livretVerse#12 { Hippolyte amoureux m’occupera sans cesse ; }
  \livretVerse#7 { Même aux Autels de la Déesse, }
  \livretVerse#12 { Je sentirai mon cœur s’élancer vers le sien. }
  \livretVerse#11 { Diane & l’univers pour moi ne sont plus rien. }
  \livretVerse#12 { Hippolyte amoureux m’occupera sans cesse, }
  \livretVerse#12 { Je vivrai pour pleurer son malheur & le mien. }
}
