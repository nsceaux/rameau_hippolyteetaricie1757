\markup\column {
  \livretPers Les Parques
  \livretVerse#8 { Du destin le vouloir suprême }
  \livretVerse#12 { A mis entre nos mains la trame de tes jours ; }
  \livretVerse#12 { Mais le fatal ciseau n’en peut trancher le cours, }
  \livretVerse#12 { Qu’au redoutable instant qu’il a marqué lui-même. }
}
