\markup\column {
  \livretPersDidas Thesée à Hippolyte
  \livretVerse#8 { Sur qui doit tomber ma colere ? }
  \livretVerse#12 { Parlez, mon fils, parlez, nommez le criminel. }
  \livretPers Hippolyte
  \livretVerse#8 { Seigneur... Dieux ! Que vais-je lui dire ? }
  \livretVerse#8 { Permettez que je me retire ; }
  \livretVerse#12 { Ou plutôt, que j’obtienne un exil éternel. }
}
