\markup\column {
  \livretPers Thesée
  \livretVerse#12 { Dieux ! Que d’infortunés gémissent dans ces lieux ! }
  \livretVerse#8 { Un seul se dérobe à mes yeux ; }
  \livretVerse#12 { Par mes cris redoublés vainement je l’appelle ; }
  \livretVerse#8 { Mes cris ne sont point entendus ; }
  \livretVerse#8 { Ah ! Montrez-moi Pyrithous ! }
  \livretVerse#12 { Craignez-vous qu’à l’aspect d’un ami si fidéle, }
  \livretVerse#8 { Ses tourmens ne soient suspendus ? }
  \livretVerse#12 { Traîne-moi jusqu’à lui, trop barbare Eumenide ; }
  \livretVerse#8 { Viens ; je prens ton flambeau pour guide. }
  \livretPers Tisiphone
  \livretVerse#12 { La mort, la seule mort a droit de vous unir. }
  \livretPers Thesée
  \livretVerse#8 { Mort propice, mort favorable, }
  \livretVerse#8 { Pour me rendre moins misérable, }
  \livretVerse#8 { Commence donc à me punir. }
}
