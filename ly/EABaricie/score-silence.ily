\markup\column {
  \livretVerse#10 { Quel doux Concerts ! Quel nouveau jour m’éclaire ! }
  \livretVerse#8 { Non, non ; ces sons harmonieux, }
  \livretVerse#8 { Ce Soleil qui brille à mes yeux, }
  \livretVerse#13 { Sans Hippolyte, helas ! Rien ne me sçauroit plaire. }
  \livretVerse#8 { Mes Yeux, vous n’êtes plus ouverts, }
  \livretVerse#6 { Que pour verser des larmes. }
  \livretVerse#12 { En vain d’aimables sons font retentir les Airs ; }
  \livretVerse#12 { Je n’ai que des soupirs, pour répondre aux Concerts, }
  \livretVerse#12 { Dont ces lieux enchantés viennent m’offrir les charmes. }
  \livretVerse#8 { Mes Yeux, vous n’êtes plus ouverts, }
  \livretVerse#6 { Que pour verser des larmes. }
}
