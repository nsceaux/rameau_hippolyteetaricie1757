\markup\column {
  \livretPers Une Chasseresse
  \livretVerse#6 { A la chasse, à la chasse, }
  \livretVerse#3 { Armez-vous. }
  \livretPers Chœur
  \livretVerse#6 { Courons tous à la chasse ; }
  \livretVerse#3 { Armons-nous. }
  \livretPers Une Chasseresse
  \livretVerse#7 { Dieu des cœurs, cédez la place ; }
  \livretVerse#7 { Non, non, ne regnez jamais. }
  \livretVerse#6 { Que Diane préside ; }
  \livretVerse#6 { Que Diane nous guide, }
  \livretVerse#6 { Dans le fond des forêts ; }
  \livretVerse#8 { Sous ses loix nous vivons en paix. }
  \livretVerse#6 { A la chasse, &c. }
  \livretPers Une Chasseresse
  \livretVerse#3 { Nos asyles }
  \livretVerse#3 { Sont tranquilles, }
  \livretVerse#7 { Non, non, rien n’a plus d’attraits. }
  \livretVerse#6 { Les plaisirs sont parfaits, }
  \livretVerse#6 { Auncun soin n’embarrasse, }
  \livretVerse#6 { On y rit des Amours, }
  \livretVerse#8 { On y passe les plus beaux jours. }
  \livretVerse#4 { A la chasse, &c. }
}
