<<
  { \key re \major
    \midiTempo #120 \digitTime\time 3/4
    \beginMark "Prélude"
    s2.*10 \bar "|."
  }
  \origLayout {
    s2.*5\break
    s2.*5\pageBreak
  }
>>
