\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trompettes" } <<
      \global \keepWithTag #'trompette \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "Tymballes" } <<
      \global \includeNotes "timbales"
    >>
  >>
  \layout { indent = \largeindent }
}
