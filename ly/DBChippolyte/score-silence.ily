\markup\column {
  \livretPers Hippolyte
  \livretVerse#12 { Le sort conduit ici ses sujets fortunés ; }
  \livretVerse#12 { Unissons-nous aux jeux qui lui sont destinés. }
}
