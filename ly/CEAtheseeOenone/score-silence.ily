\markup\column {
  \livretPers Thesée
  \livretVerse#8 { Quoi ? Tout me fuit ! Tout m’abandonne ! }
  \livretVerse#12 { Mon épouse ! Mon fils ! Ciel ! demeurez, Œnone. }
  \livretVerse#8 { C’est à vous seule à m’éclairer }
  \livretVerse#8 { Sur la trahison la plus noire. }
  \livretPers Œnone
  \livretVerse#12 { Ah ! Sauvons de la reine & les jours & la gloire. }
  \livretVerse#12 { Un desespoir affreux... pouvez-vous l’ignorer ? }
  \livretVerse#12 { Vous n’en avez été qu’un témoin trop fidéle. }
  \livretVerse#8 { Je n’ose accuser votre fils ; }
  \livretVerse#12 { Mais, la reine... Seigneur, ce fer armé contre elle, }
  \livretVerse#8 { Ne vous en a que trop appris. }
  \livretPers Thesée
  \livretVerse#9 { Dieux ! Acheve. }
  \livretPers Œnone
  \livretVerse#9 { \transparent { Dieux ! Acheve. } Un amour funeste... }
  \livretPers Thesée
  \livretVerse#10 { C’en est assez ; épargne-moi le reste. }
}
