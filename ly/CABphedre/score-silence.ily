\markup\column {
  \livretVerse#8 { Mais pourquoi tous ces vains remords ! }
  \livretVerse#12 { Ah ! Si j’en crois Arcas, mon cœur peut tout prétendre, }
  \livretVerse#8 { Thésée a vû les sombres bords. }
  \livretVerse#12 { L’Enfer, pour me punir, pourroit-il me le rendre !… }
}
