\markup\column {
  \livretPers Une Matelote
  \livretVerse#6 { L’Amour, comme Neptune, }
  \livretVerse#6 { Invite à s’embarquer ; }
  \livretVerse#6 { Pour tenter la fortune, }
  \livretVerse#6 { On ose tout risquer. }
  \livretVerse#6 { Malgré tant de naufrages, }
  \livretVerse#7 { Tous les cœurs sont matelots ; }
  \livretVerse#6 { On quitte le repos ; }
  \livretVerse#6 { On vole sur les flots ; }
  \livretVerse#7 { On affronte les orages ; }
  \livretVerse#4 { L’Amour ne dort }
  \livretVerse#4 { Que dans le Port. }
}
