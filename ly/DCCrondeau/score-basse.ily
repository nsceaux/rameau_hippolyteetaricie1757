\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Bassons" } <<
      \keepWithTag #'autres \global
      \includeNotes "basson"
    >>
    \new Staff \with { instrumentName = "B.C." } <<
      { s8 s4. s2.*7 s4\break
        s8 s4. s2.*7 s4\break }
      \keepWithTag #'autres \global
      \includeNotes "basse"
    >>
  >>
  \layout { indent = \largeindent }
}
