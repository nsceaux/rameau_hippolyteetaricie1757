\markup\column {
  \livretPers Phedre
  \livretVerse#12 { Quoi ! La terre & le ciel contre moi sont armés ! }
  \livretVerse#12 { Ma rivale me brave ! Elle suit Hippolyte ! }
  \livretVerse#12 { Ah ! Plus je vois leurs cœurs l’un pour l’autre enflamés, }
  \livretVerse#8 { Plus mon jaloux transport s’irrite. }
  \null
  \livretVerse#8 { Que rien n’échappe à ma fureur ; }
  \livretVerse#12 { Immolons à la fois l’amant & la rivale : }
  \livretVerse#8 { Haine, dépit, rage infernale, }
  \livretVerse#8 { Je vous abandonne mon cœur. }
}
