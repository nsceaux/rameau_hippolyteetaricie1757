\markup\column {
  \livretPers Aricie
  \livretVerse#12 { Ou suis-je ? de mes sens j’ai recouvré l’usage ; }
  \livretVerse#8 { Dieux, ne me l’avez-vous rendu, }
  \livretVerse#8 { Que pour me retracer l’image }
  \livretVerse#8 { Du tendre Amant que j’ai perdu ? }
}
