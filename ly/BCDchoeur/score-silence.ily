\markup\column {
  \livretPers Chœur
  \livretVerse#4 { Pluton commande ; }
  \livretVerse#5 { Vengeons notre roi. }
  \livretVerse#4 { Pluton commande ; }
  \livretVerse#4 { Suivons sa loi. }
  \livretVerse#5 { Qu’ici l’on répande }
  \livretVerse#5 { Le trouble & l’effroi. }
  \livretVerse#10 { Ne tardons pas ; les momens sont trop chers ; }
  \livretVerse#6 { Que cent gouffres ouverts }
  \livretVerse#6 { Aux regards soient offerts ; }
  \livretVerse#4 { Dans les Enfers }
  \livretVerse#3 { Que tout tremble ; }
  \livretVerse#4 { Qu’on y rassemble }
  \livretVerse#5 { Les feux & les fers. }
}
