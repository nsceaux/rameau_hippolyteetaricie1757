\markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Hippolyte
    \livretRef #'CBAphedreHippolyte
    \livretVerse#12 { Reine, sans l’ordre exprès, qui dans ces lieux m’appelle, }
    \livretVerse#12 { Quand le ciel vous ravit un époux glorieux, }
    \livretVerse#12 { Je respecterois trop votre douleur mortelle, }
    \livretVerse#12 { Pour vous montrer encore un objet odieux. }
    \livretPers Phedre
    \livretVerse#12 { Vous, l’objet de ma haine ! O ciel ! Quelle injustice ! }
    \livretVerse#8 { Je dois dissiper cette erreur ; }
    \livretVerse#12 { Helas ! Si vous croyez que Phedre vous haïsse, }
    \livretVerse#8 { Que vous connoissez mal son cœur ! }
    \livretPers Hippolyte
    \livretVerse#12 { Qu’entens-je ? A mes desirs Phedre n’est plus contraire ! }
    \livretVerse#12 { Ah ! Les plus tendres soins de votre auguste époux }
    \livretVerse#12 { Dans mon cœur désormais vont revivre pour vous. }
    \livretPers Phedre
    \livretVerse#13 { Quoi ? Prince... }
    \livretPers Hippolyte
    \livretVerse#13 { \transparent { Quoi ? Prince... } A votre fils je tiendrai lieu de Perre ; }
    \livretVerse#12 { J’affermirai son trône, & j’en donne ma foi. }
    \livretPers Phedre
    \livretVerse#12 { Vous pourriez jusques-là vous attendrir pour moi ! }
    \livretVerse#12 { C’en est trop ; & le trône, & le fils, & la mere, }
    \livretVerse#8 { Je range tout sous votre loi. }
    \livretPers Hippolyte
    \livretVerse#12 { Non ; dans l’art de regner je l’instruirai moi-même ; }
    \livretVerse#12 { Je céde sans regret la suprême grandeur. }
    \livretVerse#8 { Aricie est tout ce que j’aime ; }
    \livretVerse#12 { Et si je veux regner, ce n’est que dans sons cœur. }
    \livretPers Phedre
    \livretVerse#12 { Que dites-vous ? O Ciel ! Quelle étoit mon erreur ! }
    \livretVerse#12 { Malgré mon trône offert, vous aimez Aricie ! }
    \livretPers Hippolyte
    \livretVerse#12 { Quoi ! Votre haine encor n’est donc pas adoucie ? }
  }
  \column {
    \livretPers Phedre
    \livretVerse#8 { Tu viens d’en redoubler l’horreur... }
    \livretVerse#8 { Puis-je trop haïr ma rivale ? }
    \livretPers Hippolyte
    \livretVerse#8 { Votre rivale ! Je fremis ; }
    \livretVerse#12 { Thésée est votre époux, & vous aimez son fils ! }
    \livretVerse#13 { Ah ! Je me sens glacer d’une horreur sans égale. }
    \livretVerse#12 { Terribles ennemis des perfides humains, }
    \livretVerse#12 { Dieux, si prompts autrefois à les réduire en poudre, }
    \livretVerse#8 { Qu’attendez-vous ? Lancez la foudre. }
    \livretVerse#8 { Qui la retient entre vos mains ? }
    \livretPers Phedre
    \livretVerse#12 { Ah ! Cesse par tes vœux d’allumer le tonnerre. }
    \livretVerse#12 { Eclatte ; éveille-toi ; sors d’un honteux repos ; }
    \livretVerse#8 { Rends-toi digne fils d’un heros, }
    \livretVerse#12 { Que de monstres sans nombre a délivré la terre ; }
    \livretVerse#12 { Il n’en est échappé qu’un seul à sa fureur ; }
    \livretVerse#8 { Frappe ; ce monstre est dans mon cœur. }
    \livretPers Hippolyte
    \livretVerse#8 { Grands Dieux ! }
    \livretPers Phedre
    \livretVerse#8 { \transparent { Grands Dieux ! } Tu balances encore ! }
    \livretVerse#12 { Etouffe dans mon sang un amour que j’abhorre. }
    \livretVerse#12 { Je ne puis obtenir ce funeste secours ! }
    \livretVerse#7 { Cruel ! Quelle rigueur extrême ! }
    \livretVerse#8 { Tu me hais, autant que je t’aime ; }
    \livretVerse#8 { Mais, pour trancher mes tristes jours, }
    \livretVerse#8 { Je n’ai besoin que de moi-même. }
    \livretVerse#12 { Donne... }
    \livretPers Hippolyte
    \livretVerse#12 { \transparent { Donne... } Que faites-vous ? }
    \livretPers Phedre
    \livretVerse#12 { \transparent { Donne... Que faites-vous ? } Tu m’arraches ce fer, }
  }
}
