\piecePartSpecs
#`((dessus #:score "score-part"
           #:tag-notes violon-part
           #:tag-global part)
   (dessus2-hc #:notes "dessus"
               #:score "score-part"
               #:tag-notes violon2-part
               #:tag-global part)
   (basse #:score-template "score-basse-continue-voix"
          #:tag-notes basse-continue)
   (silence #:score "score-silence"))
