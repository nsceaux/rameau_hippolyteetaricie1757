\markup\column {
  \livretPers Chœur
  \livretVerse#6 { Chantons sur la Musette, }
  \livretVerse#2 { Chantons. }
  \livretVerse#6 { Au son qu’elle répette, }
  \livretVerse#2 { Dansons. }
  \livretVerse#5 { Que l’Echo fidèle }
  \livretVerse#5 { Rende nos chansons. }
  \livretVerse#6 { Chantons, etc. }
  \livretVerse#6 { Bergère trop cruelle, }
  \livretVerse#7 { Goûtez les tendres leçons. }
  \livretVerse#6 { Chantons sur la Musette, etc. }
}
