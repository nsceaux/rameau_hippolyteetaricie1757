\markup\column {
  \livretPers Thesée
  \livretVerse#13 { Je retrouverois chez moi ces enfers que je quitte ! }
  \livretVerse#12 { Ah ! Je céde à l’horreur dont je me sens glacer... }
  \livretVerse#12 { Dieux, détournez les maux qu’on vient de m’annoncer ; }
  \livretVerse#12 { Et surtout, prenez soin de Phedre et d’Hippolyte. }
  \livretPers Mercure
  \livretVerse#12 { Il est temps de revoir la lumiere des Cieux. }
  \livretPers Thesée
  \livretVerse#12 { Ciel ! Cachons mon retour, & trompons tous les yeux. }
}
