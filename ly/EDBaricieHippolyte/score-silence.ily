\markup\column {
  \livretPers Hippolyte et Aricie
  \livretVerse#9 { Aricie, est-ce vous que je voi. }
  \livretVerse#9 { Hippolyte, est-ce vous que je voi. }
  \livretVerse#8 { Que mon sort est digne d’envie ! }
  \livretVerse#8 { Le moment qui vous rend à moi, }
  \livretVerse#8 { Est le plus heureux de ma vie. }
}
