\markup\abs-fontsize#8 \fill-line {
  \column {
    \livretPers Aricie
    \livretRef #'DBAhippolyteAricie
    \livretVerse#12 { C’en est donc fait, cruel, rien n’arrête vos pas, }
    \livretVerse#8 { Vous desesperez votre amante. }
    \livretPers Hippolyte
    \livretVerse#12 { Helas ! Plus je vous vois, plus ma douleur augmente, }
    \livretVerse#12 { Je sens mieux tous mes maux quand je vois tant d’appas. }
    \livretPers Aricie
    \livretVerse#8 { Quoi ! L’inimitié de la Reine, }
    \livretVerse#12 { Vous fait-elle quitter l’objet de votre amour ? }
    \livretPers Hippolyte
    \livretVerse#12 { Non ! Je ne fuirois pas de cet heureux séjour }
    \livretVerse#8 { Si je n’y craignois que sa haine. }
    \livretPers Aricie
    \livretVerse#12 { Que dites-vous... }
    \livretPers Hippolyte
    \livretVerse#12 { \transparent { Que dites-vous... } Gardez d’oser porter les yeux }
    \livretVerse#8 { Sur le plus horrible mystere, }
    \livretVerse#8 { Le respect me force à me taire ; }
    \livretVerse#12 { J’offenserois le Roi, Diane, & tous les Dieux. }
    \livretPers Aricie
    \livretVerse#8 { Ah ; c’est m’en dire assez, ô crime ! }
    \livretVerse#12 { Mon cœur en est glacé d’épouvante & d’horreur. }
    \livretVerse#12 { Cependant vous partez, & de Phedre en fureur }
    \livretVerse#8 { Je vais devenir la victime. }
    \livretDidasPPage à part.
    \livretVerse#8 { Dieux ; pourquoi séparer deux cœurs }
    \livretVerse#8 { Que l’amour a faits l’un pour l’autre ! }
    \livretDidasPPage à Hippolyte.
    \livretVerse#8 { Eh ! Quelle autre main que la vôtre, }
    \livretVerse#12 { Si vous m’abandonnez, pour essuyer mes pleurs ? }
  }
  \column {
    \livretDidasPPage à part.
    \livretVerse#8 { Dieux ; pourquoi séparer deux cœurs }
    \livretVerse#8 { Que l’amour a faits l’un pour l’autre ? }
    \livretPers Hippolyte
    \livretVerse#13 { Hé bien daignez me suivre. }
    \livretPers Aricie
    \livretVerse#13 { \transparent { Hé bien daignez me suivre. } O ciel ! Que dites-vous ? }
    \livretVerse#8 { Moi vous suivre ! }
    \livretPers Hippolyte
    \livretVerse#8 { \transparent { Moi vous suivre ! } Cessez de croire }
    \livretVerse#12 { Que je puisse oublier le soin de votre gloire. }
    \livretVerse#12 { En suivant votre amant, vous suivez votre époux ; }
    \livretVerse#8 { Venez... quel silence funeste ! }
    \livretPers Aricie
    \livretVerse#11 { Ah ! Prince, croyez-en l’amour que j’en atteste, }
    \livretVerse#8 { Je ferois mon suprême bien }
    \livretVerse#8 { D’unir votre sort & le mien ; }
    \livretVerse#8 { Mais Diane est inéxorable }
    \livretVerse#8 { Pour l’amour & pour les Amans. }
    \livretPers Hippolyte
    \livretVerse#11 { A d’innocens désirs Diane est favorable }
    \livretVerse#8 { Qu’elle préside à nos sermens. }
    \livretPers Ensemble
    \livretVerse#12 { Nous allons nous jurer une immortelle foi : }
    \livretVerse#12 { Viens, Reine des Forêts, viens former notre chaîne ; }
    \livretVerse#12 { Que l’encens de nos vœux s’éleve jusqu’à toi, }
    \livretVerse#12 { Sois toujours de nos cœurs l’unique Souveraine. }
  }
}  
