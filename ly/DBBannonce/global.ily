<<
  { \tag #'autres \key re \major
    \tag #'cor \key do \major
    \tag #'cor \transposition re'
    \midiTempo #120
    \tag #'(autres cor) { \beginMark "Bruit de cors" }
    \time 6/8 s2.*4 \bar "|."
  }
  \origLayout {
    s2.*4\break
  }
>>
