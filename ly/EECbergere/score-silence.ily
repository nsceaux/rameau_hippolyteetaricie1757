\markup\column {
  \livretPers Une Bergère
  \livretVerse#5 { Plaisirs, doux Vainqueurs, }
  \livretVerse#6 { A qui tout rend les Armes, }
  \livretVerse#5 { Enchaînez les cœurs ; }
  \livretVerse#5 { Plaisirs, doux Vainqueurs, }
  \livretVerse#6 { Rassemblez tous vos charmes ; }
  \livretVerse#6 { Enchantez tous les cœurs. }
  \livretVerse#6 { Que l’Amour a d’appas ; }
  \livretVerse#6 { Regnez, ne cessez pas }
  \livretVerse#6 { De voler sur ces pas. }
  \livretVerse#5 { Plaisirs, doux Vainqueurs, etc. }
  \livretVerse#6 { C’est aux Ris, c’est au Jeux }
  \livretVerse#6 { D’embellir son Empire ; }
  \livretVerse#6 { Qu’aussi-tôt qu’on soupire, }
  \livretVerse#5 { L’on y soit heureux. }
  \livretVerse#5 { Plaisirs, doux Vainqueurs, etc. }
}
