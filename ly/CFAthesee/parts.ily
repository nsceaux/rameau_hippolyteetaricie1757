\piecePartSpecs
#`((dessus #:score "score-violons")
   (dessus2-hc #:notes "violon" #:tag-notes violon2)
   (basse #:tag-notes basse-continue
          #:score-template "score-basse-continue-voix"
          #:instrument ,#{ \markup\center-column { Toutes les basses }#})
   (silence #:score "score-silence"))
