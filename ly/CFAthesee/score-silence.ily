\markup\column {
  \livretPers Thesée
  \livretVerse#12 { Qu’ai-je appris ? Tous mes sens en sont glacez d’horreur. }
  \livretVerse#12 { Vengeons-nous ; quel projet ! Je fremis quand j’y pense. }
  \livretVerse#8 { Qu’il en va coûter à mon cœur ! }
  \livretVerse#12 { A punir un ingrat d’où vient que je balance ? }
  \livretVerse#12 { Quoi ? Ce sang, qu’il trahit, me parle en sa faveur ! }
  \livretVerse#8 { Non, non, dans un fils si coupable, }
  \livretVerse#8 { Je ne vois qu’un monstre effroyable : }
  \livretVerse#8 { Qu’il ne trouve en moi qu’un vengeur. }
}
