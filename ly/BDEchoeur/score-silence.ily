\markup\column {
  \livretPers Chœur
  \livretVerse#8 { Non, Neptune auroit beau t’entendre, }
  \livretVerse#12 { Les Enfers, malgré lui, sauroient te retenir. }
  \livretVerse#8 { On peut aisément y descendre, }
  \livretVerse#8 { Mais on ne peut en revenir. }
}
