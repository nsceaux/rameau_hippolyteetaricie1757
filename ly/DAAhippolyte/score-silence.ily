\markup\column {
  \livretPers Hippolyte
  \livretVerse#12 { Ah ! Faut-il en un jour, perdre tout ce que j’aime ! }
  \livretVerse#12 { Mon Pere pour jamais me bannit de ces lieux ; }
  \livretVerse#8 { Si cheris de Diane même, }
  \livretVerse#8 { Je ne verrai plus les beaux yeux }
  \livretVerse#8 { Qui faisoient mon bonheur suprême : }
  \livretVerse#12 { Ah ! Faut-il en un jour, perdre tout ce que j’aime ! }
}
