\piecePartSpecs
#`((dessus #:score "score-dessus")
   (parties #:score-template "score-parties")
   (dessus2-hc)
   (basse #:instrument "B.C."
          #:score-template "score-basse-continue-voix")
   (silence #:score "score-silence"))
