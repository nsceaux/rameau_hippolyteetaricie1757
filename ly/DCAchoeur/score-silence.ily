\markup\column {
  \livretPers Chœur
  \livretVerse#8 { Faisons par tout voler nos traits. }
  \livretVerse#8 { Animons-nous à la victoire ; }
  \livretVerse#8 { Que les antres les plus secrets }
  \livretVerse#8 { Retentissent de notre gloire. }
}
