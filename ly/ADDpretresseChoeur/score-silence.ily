\markup\column {
  \livretPers La Grande Prêtresse et le Chœur
  \livretVerse#8 { Dieux vengeurs, lancez le tonnerre : }
  \livretVerse#12 { Périssent les mortels qui vous livrent la guerre. }
}
