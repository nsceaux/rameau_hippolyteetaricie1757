\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus2-hc #:notes "violon2")
   (basse #:score-template "score-basse-continue-voix" #:instrument "B.C.")
   (silence #:score "score-silence"))
