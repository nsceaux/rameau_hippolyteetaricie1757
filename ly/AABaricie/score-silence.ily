\markup\column {
  \livretVerse#8 { Temple sacré, séjour tranquille, }
  \livretVerse#12 { Où Diane aujourd’hui doit recevoir mes vœux, }
  \livretVerse#12 { A mon cœur agité daigne servir d’asyle }
  \livretVerse#8 { Contre un amour trop malheureux. }
  \livretVerse#12 { Et toi, dont malgré-moi je rappelle l’image, }
  \livretVerse#12 { Cher Prince, si mes vœux ne te sont pas offerts, }
  \livretVerse#8 { Du moins, j’en apporte l’hommage }
  \livretVerse#8 { A la Déesse que tu sers. }
  \livretVerse#8 { Temple sacré, séjour tranquille, }
  \livretVerse#12 { Où Diane aujourd’hui doit recevoir mes vœux, }
  \livretVerse#12 { A mon cœur agité daigne servir d’asyle }
  \livretVerse#8 { Contre un amour trop malheureux. }
}
