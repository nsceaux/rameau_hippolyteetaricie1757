\piecePartSpecs
#`((dessus #:score "score-dessus")
   (parties)
   (dessus2-hc #:notes "violon" #:tag-notes violon2)
   (basse #:tag-notes basse-continue
          #:score-template "score-basse-continue-voix"
          #:music , #{
s1*6 <>^"[Tous]" s2. s2 <>^"[B.C.]" s4 s2.*8 \break
     #})
   (silence #:score "score-silence"))
