\markup\column {
  \livretPers Aricie
  \livretVerse#12 { Ciel ! Diane ! Malgré ma disgrace cruelle, }
  \livretVerse#8 { Signalons l’ardeur de mon zèle }
  \livretVerse#12 { Pour la Divinité qui me tient sous ses Loix. }
  \livretPers Chœur
  \livretVerse#8 { Descendez, etc. }
  \livretPers Aricie
  \livretVerse#5 { Joignons nous aux voix }
  \livretVerse#7 { De cette Troupe fidelle. }
  \livretVerse#8 { Descendez, brillante Immortelle. }
  \livretPers Chœur
  \livretVerse#8 { Regnez, etc. }
}
