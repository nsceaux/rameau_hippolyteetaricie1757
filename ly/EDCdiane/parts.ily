\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus2-hc #:notes "violon")
   (basse #:tag-notes basse-continue
          #:score "score-basse")
   (silence #:score "score-silence"))
