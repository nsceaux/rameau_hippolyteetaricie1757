\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } <<
      { s1*6 s2\break s4^\markup\whiteout "Bassons" }
      \global \keepWithTag #'basson \includeNotes "basse"
    >>
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global
      \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
