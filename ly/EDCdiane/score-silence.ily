\markup\column {
  \livretPers Diane
  \livretVerse#10 { Tendres Amans, vos malheurs sont finis ; }
  \livretVerse#8 { Pour votre Hymen tout se prépare : }
  \livretVerse#8 { Ne craignez plus qu’on vous sépare, }
  \livretVerse#6 { C’est moi qui vous unis. }
  \livretDidasP\line { Bruits de musettes }
  \livretPers Diane
  \livretVerse#8 { Les Habitans de ces retraites }
  \livretVerse#12 { Ont préparé pour vous les plus aimables jeux ; }
  \livretVerse#8 { Et déja leurs douces Musettes }
  \livretVerse#8 { Annoncent le moment heureux, }
  \livretVerse#8 { Où vous allez regner sur eux. }
}
