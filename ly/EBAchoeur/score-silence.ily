\markup\column {
  \livretPers Chœur
  \livretVerse#8 { Descendez, brillante Immortelle ; }
  \livretVerse#8 { Regnez à jamais dans nos bois. }
}
