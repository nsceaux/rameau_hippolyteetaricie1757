\markup\column {
  \livretVerse#12 { On vient de mon retour rendre grace à Neptune, }
  \livretVerse#12 { Et je voudrois encore être dans les Enfers : }
  \livretVerse#8 { Fuyons une foule importune ; }
  \livretVerse#12 { Ne puis-je disparoître aux yeux de l’univers ! }
}
