\markup\column {
  \livretPers Phedre
  \livretVerse#8 { Cruelle Mere des Amours, }
  \livretVerse#12 { Ta vengeance a perdu ma trop coupable race, }
  \livretVerse#8 { N’en suspendras-tu point le cours ? }
  \livretVerse#12 { Ah ! Du moins, à tes yeux, que Phedre trouve grace. }
  \livretVerse#8 { Je ne te reproche plus rien, }
  \livretVerse#12 { Si tu rends à mes vœux Hippolyte sensible ; }
  \livretVerse#12 { Mes feux me font horreur, mais mon crime est le tien ; }
  \livretVerse#8 { Tu dois cesser d’être inflexible. }
  \livretVerse#8 { Cruelle Mere des Amours, etc. }
}
