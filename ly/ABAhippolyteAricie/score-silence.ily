\markup\column {
  \livretPers Hippolyte
  \livretVerse#12 { Princesse, quels apprêts me frappent dans ce Temple ! }
  \livretPers Aricie
  \livretVerse#7 { Diane préside en ces lieux ; }
  \livretVerse#12 { Lui consacrer mes jours, c’est suivre votre exemple. }
  \livretPers Hippolyte
  \livretVerse#12 { Non, vous les immolez, ces jours si précieux. }
  \livretPers Aricie
  \livretVerse#12 { J’exécute du Roi la volonté suprême ; }
  \livretVerse#10 { A Thésée, à son Fils, ces jours sont odieux. }
  \livretPers Hippolyte
  \livretVerse#12 { Moi, vous haïr ! O Ciel ! Quelle injustice extrême ! }
  \livretPers Aricie
  \livretVerse#12 { Je ne suis point l’objet de votre inimitié ? }
  \livretPers Hippolyte
  \livretVerse#8 { Je sens pour vous une pitié }
  \livretVerse#8 { Aussi tendre que l’amour même. }
  \livretPers Aricie
  \livretVerse#9 { Quoi ? Le fier Hippolyte… }
  \livretPers Hippolyte
  \livretVerse#9 { \transparent { Quoi ? Le fier Hippolyte… } Hélas ! }
  \livretVerse#12 { Je n’en ai que trop dit ; je ne m’en repens pas, }
  \livretVerse#8 { Si vous avez daigné m’entendre : }
  \livretVerse#12 { Mon trouble, mes soûpirs, vos malheurs, vos appas, }
  \livretVerse#12 { Tout vous annonce un cœur trop sensible & trop tendre. }
  \livretPers Aricie
  \livretVerse#8 { Ah ! Que venez-vous de m’apprendre ! }
  \livretVerse#12 { C’en est fait ; pour jamais mon repos est perdu. }
  \livretVerse#8 { Peut-être votre indifférence }
  \livretVerse#8 { Tôt ou tard me l’auroit rendu ; }
  \livretVerse#10 { Mais votre amour m’en ôte l’esperance. }
  \livretVerse#12 { C’en est fait ; pour jamais mon repos est perdu. }
  \livretPers Hippolyte
  \livretVerse#12 { Qu’entends-je ! Quel transport de mon ame s’empare ! }
  \livretPers Aricie
  \livretVerse#7 { Oubliez-vous qu’on nous sépare ! }
  \livretVerse#12 { Quel temple redoutable, & quel affreux lien ! }
}
