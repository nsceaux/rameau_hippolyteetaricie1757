\piecePartSpecs
#`((dessus #:instrument "Violons")
   (parties #:score-template "score-parties")
   (dessus2-hc)
   (basse #:score-template "score-basse-continue-voix"
          #:indent 0
          #:tag-notes basse-continue)
   (silence #:score "score-silence" #:tag-global silence))
