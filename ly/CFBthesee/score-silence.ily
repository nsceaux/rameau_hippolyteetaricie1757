\markup\column {
  \livretVerse#12 { Puissant maître des flots, favorable Neptune, }
  \livretVerse#8 { Entens ma gémissante voix ; }
  \livretVerse#8 { Permets que ton fils t’importune, }
  \livretVerse#6 { Pour la derniere fois. }
  \livretVerse#12 { Hippolyte m’a fait le plus sanglant outrage ; }
  \livretVerse#8 { Rempli le serment qui t’engage ; }
  \livretVerse#12 { Préviens par son trépas un desespoir affreux ; }
  \livretVerse#12 { Ah ! Si tu refusois de venger mon injure, }
  \livretVerse#12 { Je serois parricide, & tu serois parjure ; }
  \livretVerse#8 { Nous serions coupables tous deux. }
}
