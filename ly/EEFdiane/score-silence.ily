\markup\column {
  \livretPers Diane
  \livretVerse#12 { Bergers, vous allez voir combien je suis fidèle }
  \livretVerse#8 { A tenir ce que je promets ; }
  \livretVerse#12 { Le Heros, qui sur vous va regner desormais, }
  \livretVerse#8 { Sera le prix de votre zèle. }
}
