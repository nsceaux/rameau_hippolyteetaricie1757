\markup\column {
  \livretPers Aricie
  \livretVerse#12 { Rossignols amoureux, répondez à nos voix ; }
  \livretVerse#8 { Par la douceur de vos ramages, }
  \livretVerse#8 { Rendez les plus tendres hommages }
  \livretVerse#12 { A la Divinité qui regne dans nos Bois. }
}
