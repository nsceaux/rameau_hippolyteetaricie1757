\markup\column {
  \livretPers Aricie et Hippolyte
  \livretVerse#8 { Nous brûlons des plus pures flammes, }
  \livretVerse#12 { L’Amour n’offre à nos cœurs que d’innocens appas, }
  \livretVerse#6 { Tu ne le défends pas, }
  \livretVerse#8 { Non, non, tu ne le défends pas }
  \livretVerse#12 { Quand c’est par la vertu qu’il regne sur nos ames. }
}
