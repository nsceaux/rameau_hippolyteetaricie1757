\markup\column {
  \livretPers Chœur
  \livretVerse#10 { Quel bruit ! Quels vents ! Quelle montagne humide ! }
  \livretVerse#8 { Quel monstre elle enfante à nos yeux ? }
  \livretVerse#12 { O Diane, accourez ; volez du haut des cieux. }
  \livretPersDidas Hippolyte s’avance vers le monstre.
  \livretVerse#12 { Venez, qu’à son défaut je vous serve de guide. }
  \livretPers Aricie
  \livretVerse#2 { Arrête. }
}
