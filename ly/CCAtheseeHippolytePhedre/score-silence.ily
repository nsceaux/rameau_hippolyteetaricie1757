\markup\column {
  \livretPers Thesée
  \livretVerse#8 { Que vois-je ? Quel affreux spectacle ! }
  \livretPers Hippolyte
  \livretVerse#12 { Mon pere ! }
  \livretPers Phedre
  \livretVerse#12 { \transparent { Mon pere ! } Mon époux. }
  \livretPersDidas Thesée à part
  \livretVerse#12 { \transparent { Mon pere ! Mon époux. } O trop fatal Oracle ! }
  \livretVerse#12 { Je trouve les malheurs que ma prédits l’Enfer. }
  \livretDidasP\line { à Phedre. }
  \livretVerse#12 { Reine, dévoilez-moi ce funeste mystére. }
  \livretPersDidas Phedre à Thesée
  \livretVerse#12 { N’approchez point de moi ; l’Amour est outragé ; }
  \livretVerse#6 { Que l’Amour soit vengé. }
}
