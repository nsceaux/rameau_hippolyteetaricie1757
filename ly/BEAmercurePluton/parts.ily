\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus2-hc #:notes "violon2")
   (basse #:notes "basse-continue"
          #:score-template "score-basse-continue-voix")
   (silence #:score "score-silence"))
