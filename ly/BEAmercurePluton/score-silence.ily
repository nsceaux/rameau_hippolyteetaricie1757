\markup\column {
  \livretPersDidas Mercure à Pluton
  \livretVerse#8 { Neptune vous demande grace }
  \livretVerse#8 { Pour un Fils trop audacieux. }
  \livretPers Pluton
  \livretVerse#12 { N’a-t’il pas partagé son crime & son audace, }
  \livretVerse#12 { En ouvrant sous ses pas la route de ces lieux ? }
  \livretVerse#12 { Non, non ; je dois punir un Mortel qui m’offense. }
  \livretPers Mercure
  \livretVerse#12 { Jupiter tient les Cieux sous son obéïssance, }
  \livretVerse#8 { Neptune régne sur les mers ; }
  \livretVerse#12 { Pluton peut, à son gré, signaler sa vengeance }
  \livretVerse#8 { Dans le noir séjour des enfers ; }
  \livretVerse#8 { Mais le bonheur de l’univers }
  \livretVerse#8 { DépenD de votre intelligence. }
  \livretPers Pluton
  \livretVerse#12 { C’en est fait, je me rends ; sur mon juste courroux, }
  \livretVerse#8 { Le bien de l’univers l’emporte. }
  \livretVerse#12 { De l’infernale nuit que ce coupable sorte ; }
  \livretVerse#12 { Peut-être son destin n’en sera pas plus doux. }
}
