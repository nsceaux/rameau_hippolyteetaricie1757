\markup\column {
  \livretPers Chœur
  \livretVerse#8 { Que ce rivage retentisse }
  \livretVerse#8 { De la gloire du Dieu des flots : }
  \livretVerse#8 { Qu’à ses bienfaits tout applaudisse ; }
  \livretVerse#12 { Il rend à l’univers le plus grand des heros. }
  \livretVerse#8 { Que ce rivage retentisse }
  \livretVerse#8 { De la gloire du Dieu des flots. }
}
