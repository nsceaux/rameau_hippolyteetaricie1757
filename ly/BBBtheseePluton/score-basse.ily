\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiri instrumentName = "Bassons" } <<
      { %% bassons + basses
        s1*9 s1 s2. s1*4 s2 \break \startHaraKiri
        %% B.C.
        \clef "basse" s2 s1*3 s1*7 s1*3 s1 s1*5 s1*4 \break
        %% Air vif
        s1*22 \break
        s1*5 s1 s2. s1 s1 s2. s1 \break }
      \global \keepWithTag #'bassons \includeNotes "basse"
    >>
    \new Staff \with { \haraKiriFirst } <<
      { %% bassons + basses
        \startHaraKiri s1*9 s1 s2. s1*4 s2 \break
        %% B.C.
        \clef "basse" s2 s1*3 s1*7 s1*3 s1 s1*5 s1*4 \break
        %% Air vif
        s1*22 \break
        s1*5 s1 s2. s1 s1 s2. s1 \break \stopHaraKiri
        s1^\markup\whiteout "Basse du grand chœur" }
      \global \keepWithTag #'basse \includeNotes "basse"
    >>
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff \with {
      \haraKiri
      instrumentName = \markup\center-column { B.C. et contre basse }
    } <<
      { s1*9 s1 s2. s1*4 s1*4 s1*7 s1*3 s1 s1*5 s1*4
        s1*22 s1*5 s1 s2. s1 s1 s2. s1
        <>_\markup\whiteout "Bassons et basse continue"
        s1*27\break }
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \largeindent }
}
