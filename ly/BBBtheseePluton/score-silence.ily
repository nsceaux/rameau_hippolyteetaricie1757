\markup\column {
  \livretPers Thesée
  \livretVerse#12 { Inexorable Roi de l’empire infernal, }
  \livretVerse#8 { Digne Frere, & digne Rival }
  \livretVerse#8 { Du Dieu qui lance le tonnerre, }
  \livretVerse#12 { Est-ce donc pour venger tant de monstres divers, }
  \livretVerse#8 { Dont ce bras a purgé la terre, }
  \livretVerse#12 { Que l’on me livre en proie aux monstres des Enfers ? }
  \livretPers Pluton
  \livretVerse#12 { Si tes exploits sont grands, voi quelle en est la gloire ; }
  \livretVerse#12 { Ton nom sur le trépas remporte la victoire ; }
  \livretVerse#8 { Comme nous il est immortel ; }
  \livretVerse#12 { Mais, d’une égale main, puisqu’il faut qu’on dispense }
  \livretVerse#8 { Et la peine & la récompense, }
  \livretVerse#12 { N’attens plus de Pluton qu’un tourment éternel. }
  \livretVerse#12 { D’un trop coupable ami, trop fidéle complice, }
  \livretVerse#8 { Tu dois partager son supplice. }
  \livretPers Thesée
  \livretVerse#8 { Je consens à le partager ; }
  \livretVerse#12 { L’amitié qui nous joint m’en fait un bien suprême. }
  \livretVerse#12 { Non, de Pyrithous tu ne peux te vanger, }
  \livretVerse#6 { Sans me punir moi-même. }
  \livretVerse#12 { Sous les drapeaux de Mars, unis par la valeur, }
  \livretVerse#12 { Je l’ai vû sur mes pas voler à la victoire. }
  \livretVerse#8 { Je dois partager son malheur, }
  \livretVerse#12 { Comme il a partagé mes périls & ma gloire. }
  \livretPers Pluton
  \livretVerse#12 { Mais cette gloire enfin, falloit-il la ternir ? }
  \livretVerse#12 { Parle. Le crime même a-t’il dû vous unir ? }
  \livretPers Thesée
  \livretVerse#8 { Le péril d’un ami si tendre, }
  \livretVerse#12 { Aux Enfers, avec lui, m’a contraint à descendre ; }
  \livretVerse#12 { Est-ce là le forfait que tu prétends punir ? }
  \livretVerse#8 { Pour prix d’un projet téméraire, }
  \livretVerse#12 { Ton malheureux rival éprouve ta colere ; }
  \livretVerse#12 { Mais, trop fatal Vengeur, de quoi me punis-tu ? }
  \livretVerse#8 { Ah ! Si son amour est un crime, }
  \livretVerse#8 { L’amitié qui pour lui m’anime }
  \livretVerse#8 { N’est-elle pas une vertu ? }
  \livretPers Pluton
  \livretVerse#8 { Eh bien je remets ma victime }
  \livretVerse#12 { Aux juges souverains de l’Empire des Morts ; }
  \livretVerse#12 { Va, sors ; en attendant un arrêt légitime, }
  \livretVerse#8 { Je t’abandonne à tes remords. }
}
