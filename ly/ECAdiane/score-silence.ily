\markup\column {
  \livretPers Diane
  \livretVerse#8 { Et vous : Troupe à ma voix fidelle, }
  \livretVerse#8 { Doux Zephirs, volez en ces lieux ; }
  \livretVerse#12 { Il est temps d’apporter le dépôt précieux }
  \livretVerse#8 { Que j’ai commis à vôtre zéle. }
}
