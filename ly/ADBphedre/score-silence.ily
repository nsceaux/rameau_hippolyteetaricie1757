\markup\column {
  \livretPers Phedre
  \livretVerse#8 { Périsse la vaine puissance }
  \livretVerse#8 { Qui s’éleve contre les Rois : }
  \livretVerse#8 { Tremblez ; redoutez ma vengeance, }
  \livretVerse#12 { Et le Temple & l’Autel vont tomber à ma voix. }
  \livretVerse#12 { Tremblez, j’ai sû prévoir la désobéïssance ; }
  \livretVerse#8 { Périsse la vaine puissance, }
  \livretVerse#8 { Qui s’éleve contre les Rois. }
}
