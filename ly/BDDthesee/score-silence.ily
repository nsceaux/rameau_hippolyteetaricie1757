\markup\column {
  \livretVerse#8 { Puisque Pluton est infléxible, }
  \livretVerse#12 { Dieu des mers, c’est à toi qu’il me faut recourir ; }
  \livretVerse#12 { Que ton fils, dans son pere, éprouve un cœur sensible, }
  \livretVerse#12 { Trois fois dans mes malheurs tu dois me secourir ; }
  \livretVerse#8 { Le fleuve, aux Dieux mêmes terrible, }
  \livretVerse#12 { Et qu’ils n’osent jamais attester vainement, }
  \livretVerse#8 { Le Styx a reçu ton serment : }
  \livretVerse#12 { Au premier de mes vœux tu viens d’être fidèle ; }
  \livretVerse#8 { Tu m’as ouvert l’affreux séjour, }
  \livretVerse#8 { Où règne une nuit éternelle ; }
  \livretVerse#8 { Grand Dieu, daigne me rendre au jour. }
}
