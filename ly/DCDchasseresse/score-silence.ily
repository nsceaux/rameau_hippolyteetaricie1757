\markup\column {
  \livretPers Une Chasseresse
  \livretVerse#8 { Amans, quelle est votre foiblesse ? }
  \livretVerse#9 { Voyez ! L’Amour sans vous allarmer ; }
  \livretVerse#8 { Ces mêmes traits dont il vous blesse, }
  \livretVerse#9 { Contre nos cœurs n’osent plus s’armer. }
  \livretVerse#4 { Malgré ses charmes }
  \livretVerse#3 { Les plus doux, }
  \livretVerse#4 { Bravez ses armes, }
  \livretVerse#5 { Faites comme nous ; }
  \livretVerse#5 { Osez, sans allarmes, }
  \livretVerse#5 { Attendre ses coups ; }
  \livretVerse#11 { Si vous combattez, la victoire est à vous, }
  \livretVerse#8 { Amans, quelle est votre foiblesse ? }
  \livretVerse#9 { Voyez ! L’Amour sans vous allarmer ; }
  \livretVerse#8 { Ces mêmes traits dont il vous blesse, }
  \livretVerse#9 { Contre nos cœurs n’osent plus s’armer. }
  \livretVerse#9 { Vous vous plaignez qu’il a des rigueurs, }
  \livretVerse#10 { Et vous aimez tous les traits qu’il vous lance ! }
  \livretVerse#8 { C’est vous qui les rendez vainqueurs ; }
  \livretVerse#5 { Pourquoi sans défense }
  \livretVerse#4 { Livrer vos cœurs ? }
  \livretVerse#8 { Amans, quelle est votre foiblesse, &c. }
}
