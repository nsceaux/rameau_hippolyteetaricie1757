\piecePartSpecs
#(let ((breaks #{ s2 s2.*7 s4\break s2 s2.*7 s4\break #}))
       `((dessus #:instrument "Un hautbois"
                 #:music ,breaks
                 #:notes "hautbois")
         (basse
          #:clef "tenor"
          #:music ,breaks
          #:instrument ,#{ \markup\center-column { Un basson et B.C. } #}
          #:score-template "score-basse-continue")
         (silence #:score "score-silence"
                  #:music ,breaks)))
