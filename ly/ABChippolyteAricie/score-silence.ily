\markup\column {
  \livretPers Hippolyte
  \livretVerse#12 { Je vous affranchirai d’une loi si cruelle. }
  \livretPers Aricie
  \livretVerse#12 { Phédre sur sa captive à des droits absolus ; }
  \livretVerse#12 { Que sert de nous aimer ? Nous ne nous verrons plus. }
  \livretPers Hippolyte
  \livretVerse#12 { O Diane ! Protége une flamme si belle. }
}
