\markup\column {
  \livretPersDidas Pluton descendu de son trône.
  \livretVerse#12 { Qu’à servir mon couroux tout l’Enfer se prépare ; }
  \livretVerse#8 { Que l’Averne, que le Tenare, }
  \livretVerse#8 { Le Cocyte, le Phlegeton, }
  \livretVerse#8 { Par ce qu’ils ont de plus barbare, }
  \livretVerse#8 { Vengent Proserpine & Pluton. }
  \livretPers Chœur
  \livretVerse#8 { Que l’Averne, etc. }
}
