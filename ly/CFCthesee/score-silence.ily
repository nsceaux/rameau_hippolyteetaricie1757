\markup\column {
  \livretVerse#8 { Mais de courroux l’onde s’agite. }
  \livretVerse#13 { Tremble ; tu vas périr, trop coupable Hippolyte. }
  \livretVerse#12 { Le sang a beau crier, je n’entens plus sa voix. }
  \livretVerse#12 { Tout s’apprête à punir une offense mortelle ; }
  \livretVerse#8 { Neptune me sera fidéle, }
  \livretVerse#8 { C’est aux Dieux à venger les Rois. }
}
