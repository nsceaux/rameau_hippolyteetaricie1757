\markup\column {
  \livretPers Thesée
  \livretVerse#12 { Ah ! Qu’on daigne du moins, en m’ouvrant les Enfers, }
  \livretVerse#8 { Rendre un vengeur à l’univers. }
}
