\clef "vbasse" do'4. mib'8 si4\trill si8 re' |
sol4 sol8 lab lab\circA lab re4 |
\appoggiatura re8 mib4 sol8 la16 si do'4~ do'16\circA do' do' fa |
sol2 r |
