\markup\column {
  \livretVerse#7 { Dieu d’Amour, pour nos asyles, }
  \livretVerse#7 { Tes tourmens ne sont pas faits. }
  \livretVerse#7 { Tous les cœurs y sont tranquilles, }
  \livretVerse#7 { Tes efforts sont inutiles ; }
  \livretVerse#9 { Non, non, tu n’en peux troubler la paix. }
  \livretVerse#3 { Tes allarmes }
  \livretVerse#3 { Ont des charmes }
  \livretVerse#7 { Pour qui manque de raison }
  \livretVerse#3 { Mais nos ames }
  \livretVerse#3 { De tes flammes }
  \livretVerse#7 { Reconnoissent le poison : }
  \livretVerse#6 { Va, fuis ; pers l’esperance : }
  \livretVerse#6 { Va, fuis loin de nos cœurs : }
  \livretVerse#7 { Tu n’as point de traits vainqueurs. }
}
