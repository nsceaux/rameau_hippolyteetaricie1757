\markup\column {
  \livretPersDidas Diane à ses Prêtresses
  \livretRef #'AEAdianeAricieHippolyte
  \livretVerse#12 { Ne vous allarmez pas d’un projet téméraire, }
  \livretVerse#10 { Tranquilles cœurs, qui vivez sous ma loi. }
  \livretVerse#12 { Vous voyez Jupiter se déclarer mon Pere ; }
  \livretVerse#8 { Sa foudre vole devant moi. }
  \livretDidasP\line { à Phedre }
  \livretVerse#8 { Toi, tremble Reine sacrilege ; }
  \livretVerse#12 { Penses-tu m’honorer par d’unjustes rigueurs ? }
  \livretVerse#7 { Apprens que Diane protége }
  \livretVerse#6 { La liberté des cœurs. }
  \livretDidasP\line { à Aricie }
  \livretVerse#12 { Et toi, triste victime, à me suivre fidéle, }
  \livretVerse#12 { Fais toujours expirer les monstres sous tes traits. }
  \livretVerse#12 { On peut servir Diane avec le même zéle, }
  \livretVerse#8 { Dans son temple & dans les forêts. }
  \livretPers Hippolyte et Aricie
  \livretVerse#12 { Déesse, pardonnez... }
  \livretPers Diane
  \livretVerse#12 { \transparent { Déesse, pardonnez... } Votre vertu m’est chere ; }
  \livretVerse#12 { Et c’est au crime seul que je dois ma colere. }
}
