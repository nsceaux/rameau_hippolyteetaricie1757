\clef "vdessus" R1 |
sol''2. mib''8 mib'' |
sib'2\circA sib'4 sib'8 do'' |
\appoggiatura do'' re''2 re''8 r r4 |
R1*4 |
R1 |
R1*2 |
r2 re''4 re'' |
sol''2\circA sol''4. do''8 |
\appoggiatura do'' re''2\circA re''8 r r4 |
r2 sib'4. sib'8 |
\appoggiatura sib' la'2\circA la'4 sib' |
\appoggiatura la'8 sol'2. 