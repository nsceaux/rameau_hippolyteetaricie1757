\piecePartSpecs
#`((dessus #:tag-notes violons)
   (parties #:score-template "score-parties")
   (dessus2-hc)
   (basse #:score-template "score-basse-continue-voix")
   (silence #:score "score-silence" #:tag-global silence))
