\markup\column {
  \livretPers Chœur
  \livretVerse#8 { Dieux ! Quelle flamme l’environne ! }
  \livretPers Aricie
  \livretVerse#12 { Quel nuage épais ! Tout se dissipe ; hélas ? }
  \livretVerse#8 { Hippolyte ne paroît pas. }
  \livretVerse#2 { Je meurs. }
  \livretDidasP\line { \smallCaps Aricie tombe évanouie. }
  \livretPers Chœur
  \livretVerse#6 { O disgrace cruelle ! }
  \livretVerse#6 { Hippolyte n’est plus. }
}
