\markup\column {
  \livretVerse#7 { Dans ce paisible séjour, }
  \livretVerse#7 { Regne l’aimable innocence : }
  \livretVerse#7 { Les traits que lance l’Amour }
  \livretVerse#7 { Sur nous n’ont point de puissance ; }
  \livretVerse#7 { Nous jouissons à jamais }
  \livretVerse#7 { des doux charmes de la paix. }
}
