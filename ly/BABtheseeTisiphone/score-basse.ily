\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new GrandStaff <<
      \new Staff \with { \haraKiriFirst } <<
        { \startHaraKiri
          s1*3\break s4.*37\break
          s1 s2. s1 s1*3 s1 s2. s1*3 s1 s1 s1 s2 s2 s2.*5 s1*3
          s1 s1 s2. s1 s1 s2.*2 \break \stopHaraKiri
          s1^\markup\whiteout { \concat { P \super er } Basson } }
        \global \keepWithTag #'basson1 \includeNotes "basse" >>
      \new Staff \with { \haraKiriFirst } <<
        { s1*3 s4.*37 s1 s2. s1 s1*3 s1 s2. s1*3 s1 s1 s1 s2 s2 s2.*5 s1*3
          s1 s1 s2. s1 s1 s2.*2
          s1^\markup\whiteout { \concat { 2 \super e } Basson } }
        \global \keepWithTag #'basson2 \includeNotes "basse" >>
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Bassons Basses }
    } <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { indent = \largeindent }
}
