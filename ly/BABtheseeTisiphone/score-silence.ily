\markup\column {
  \livretPers Thesée
  \livretVerse#12 { Laisse-moi respirer, implacable Furie. }
  \livretPers Tisiphone
  \livretVerse#8 { Non, dans le séjour ténébreux }
  \livretVerse#12 { C’est en vain qu’on gémit ; c’est en vain que l’on crie ; }
  \livretVerse#8 { Et les plaintes des malheureux }
  \livretVerse#8 { Irritent notre barbarie. }
  \livretPers Thesée
  \livretVerse#12 { Dieux ! N’est-ce pas assez des maux que j’ai soufferts ? }
  \livretVerse#12 { J’ai vû Pyrithous déchiré par Cerbere ; }
  \livretVerse#12 { Jai vû ce monstre affreux trancher des jours si chers, }
  \livretVerse#12 { Sans daigner dans mon sang assouvir sa colere. }
  \livretVerse#8 { J’attendois la mort sans effroi, }
  \livretVerse#8 { Et la mort fuyoit loin de moi. }
  \livretPers Tisiphone
  \livretVerse#8 { Eh ! Croyois-tu que de tes peines }
  \livretVerse#12 { Le moment de ta mort fut le dernier instant ? }
  \livretVerse#12 { Pirithous gémit sous d’éternelles chaînes ; }
  \livretVerse#8 { Tremble ; le même sort t’attend. }
  \livretPers Thesée
  \livretVerse#8 { Ah ! Qu’avec lui je le partage, }
  \livretVerse#8 { Ce sort que tu viens m’annoncer, }
  \livretVerse#12 { Rends-moi Pirithous, je me livre à ta rage ; }
  \livretVerse#12 { Mais sur lui, s’il se peut, cesse de l’exercer. }
  \livretPers Tisiphone et Thesée
  \livretVerse#8 { C’est peu pour moi d’une victime }
  \livretVerse#8 { Contente-toi d’une victime }
  \livretVerse#8 { Non, rien n’apaise ma fureur }
  \livretVerse#8 { Quoi ? Rien n’appaise ta fureur ! }
  \livretVerse#12 { Je dois porter partout le ravage & l’horreur }
  \livretVerse#12 { Dois-tu porter partout le ravage & l’horreur, }
  \livretVerse#8 { Lorsque partout je vois le crime. }
  \livretVerse#8 { Quand sur moi seul je prends le crime ! }
}
