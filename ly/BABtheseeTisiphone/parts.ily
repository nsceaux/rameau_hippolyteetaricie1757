\piecePartSpecs #`((dessus #:score "score-violons")
                   (dessus2-hc #:notes "dessus" #:tag-notes violon2)
                   (basse #:score "score-basse")
                   (silence #:score "score-silence"))
