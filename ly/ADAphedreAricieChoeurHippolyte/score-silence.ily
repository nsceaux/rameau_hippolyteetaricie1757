\markup\column {
  \livretPersDidas Phedre à Aricie
  \livretRef #'ADAphedreAricieChoeurHippolyte
  \livretVerse#12 { Princesse, ce grand jour par des nœuds éternels }
  \livretVerse#8 { Va vous unir aux Immortels. }
  \livretPers Aricie
  \livretVerse#8 { Je crains que le ciel ne condamne }
  \livretVerse#13 { L’hommage que j’apporte aux pieds des saints autels. }
  \livretVerse#8 { Quel cœur viens-je offrir à Diane ! }
  \livretPers Phedre
  \livretVerse#12 { Quel discours ! }
  \livretPers Aricie
  \livretVerse#12 { \transparent { Quel discours ! } Sans remors, comment puis-je en ces lieux, }
  \livretVerse#8 { Offrir un cœur que l’on opprime ? }
  \livretPers Chœur des Prêtresses
  \livretVerse#12 { Non, non, un cœur forcé n’est pas digne des Dieux ; }
  \livretVerse#8 { Le sacrifice en est un crime. }
  \livretPers Phedre
  \livretVerse#12 { Quoi ? L’on ose braver le suprême pouvoir ! }
  \livretPers Chœur
  \livretVerse#12 { Obéïssez au Dieux ; c’est le premier devoir. }
  \livretPersDidas Phedre à Hippolyte
  \livretVerse#8 { Prince, vous souffrez qu’on outrage }
  \livretVerse#8 { Et votre Pere, & votre Roi ! }
  \livretPersDidas Hippolyte à Phedre
  \livretVerse#12 { Vous sçavez que respect à Diane m’engage ; }
  \livretVerse#12 { Dès mes plus tendres ans je lui donnai ma foi. }
  \livretPers Phedre
  \livretVerse#12 { Dieux ! Thésée en son fils trouve un sujet rebelle ! }
  \livretPers Hippolyte
  \livretVerse#8 { Je sais tout ce que je lui doi ; }
  \livretVerse#12 { Mais, ne puis-je pour lui faire éclatter mon zéle, }
  \livretVerse#8 { Qu’en outrageant une Immortelle ? }
  \livretPers Phedre
  \livretVerse#8 { Laissez des détours superflus ; }
  \livretVerse#12 { La vertu quelquefois sert de prétexte au crime. }
  \livretPers Hippolyte
  \livretVerse#12 { Quel crime ! }
  \livretPers Phedre
  \livretVerse#12 { \transparent { Quel crime ! } Je ne sais qui vous touche le plus, }
  \livretVerse#8 { De l’autel, ou de la victime. }
  \livretPers Hippolyte
  \livretVerse#8 { Du moins, par d’injustes rigueurs, }
  \livretVerse#8 { Je ne sais point forcer les cœurs. }
}
