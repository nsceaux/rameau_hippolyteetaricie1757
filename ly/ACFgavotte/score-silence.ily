\markup\column {
  \livretPersDidas La Grande Prêtresse alternativement avec le chœur
  \livretVerse#7 { De l’amour fuyez les charmes }
  \livretVerse#7 { Craignez jusqu’à ses douceurs, }
  \livretVerse#7 { De fleurs il couvre ses armes, }
  \livretVerse#3 { Mais les larmes, }
  \livretVerse#3 { Les allarmes, }
  \livretVerse#7 { Sont le prix des tendres cœurs. }
  \livretPersDidas La Grande Prêtresse et le Chœur alternativement avec le chœur
  \livretVerse#7 { La paix & l’indifférence }
  \livretVerse#7 { Comblent ici nos désirs ; }
  \livretVerse#7 { Les biens que l’amour dispense }
  \livretVerse#7 { Coûtent toujours des soûpirs ; }
  \livretVerse#7 { Dans le sein de l’innocence }
  \livretVerse#7 { Nous trouvons les vrais plaisirs. }
}
