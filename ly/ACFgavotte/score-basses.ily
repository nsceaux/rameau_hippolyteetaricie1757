\score {
  \new StaffGroupNoBar <<
    \new Staff \with { \haraKiriFirst \tinyStaff } \withLyrics <<
      \global \keepWithTag #'pretresse \includeNotes "voix"
    >> \keepWithTag #'pretresse \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } <<
      { \startHaraKiri s2 s1*9 s2\break
        s2 s1*19 s2 \stopHaraKiri \break
        s2 s1*11 s2 \startHaraKiri }
      \global \keepWithTag #'basson \includeNotes "basse" >>
    \new Staff \with { instrumentName = \markup\center-column { Bassons Basses } } <<
      \global \keepWithTag #'basse-continue \includeNotes "basse" >>
  >>
  \layout { indent = \largeindent }
}
