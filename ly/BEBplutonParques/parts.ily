\piecePartSpecs
#`((dessus #:score "score-violons")
   (parties #:score-template "score-parties")
   (dessus2-hc #:notes "violon2")
   (basse #:score-template "score-basse-continue-voix")
   (silence #:score "score-silence" #:tag-global silence))
