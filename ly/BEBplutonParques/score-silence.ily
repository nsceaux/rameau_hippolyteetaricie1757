\markup\column {
  \livretPers Pluton
  \livretVerse#12 { Vous, qui de l’avenir percez la nuit profonde, }
  \livretVerse#12 { Qui tenez dans vos mains & la vie & la mort, }
  \livretVerse#8 { Vous qui reglez le sort du monde, }
  \livretVerse#8 { Parques, annoncez-lui son sort. }
  \livretPers Les Trois Parques
  \livretVerse#13 { Quelle soudaine horreur ton destin nous inspire ? }
  \livretVerse#12 { Où cours-tu, Malheureux ? Tremble ; frémis d’effroi. }
  \livretVerse#8 { Tu sors de l’infernal empire, }
  \livretVerse#8 { Pour trouvez les Enfers chez toi. }
}
