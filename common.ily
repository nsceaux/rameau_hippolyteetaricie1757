\header {
  copyrightYear = "2007"
  composer = "Jean-Philippe Rameau"
  poet = "Simon-Joseph Pellegrin"
  date = "Version de 1757"
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes #t)
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'print-footnotes
                (not (symbol? (ly:get-option 'part))))
%% Staff size
#(set-global-staff-size (if (symbol? (ly:get-option 'part)) 20 16))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

%% No key signature modification
#(ly:set-option 'forbid-key-modification #t)

%% Tremolo for string instruments
#(if (memq (ly:get-option 'part) '(dessus parties basse basse-continue))
     (ly:set-option 'use-tremolo-repeat #t))

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"

\opusPartSpecs
#`(;; Dessus de violons, flutes, hautbois
   (dessus "Violons, Flûtes, Hautbois" ()
           (#:notes "dessus" #:tag-notes dessus))
   (dessus2-hc "Dessus II & haute-contre" ()
               (#:notes "haute-contre" #:tag-notes haute-contre #:clef "treble"))
   (trompette-timbales "Trompette et timbales" ()
                       (#:score "score-tt"))
   (cor "Cors en ré" () (#:notes "cor"))
   (parties "Hautes-contre et tailles" ()
            (#:notes "parties" #:tag-notes parties #:clef "alto"))
   (basse "Basse continues, basses et Bassons" ()
          (#:notes "basse" #:clef "bass" #:tag-notes basse)))
   
\opusTitle "Hippolyte et Aricie"


%%%%%%%%%

scene =
#(define-music-function (parser location title toc-title) (string? markup?)
  (add-toc-item parser 'tocSceneMarkup (if (and (string? toc-title)
                                                (string-null? toc-title))
                                           (string-upper-case title)
                                           toc-title))
  (add-odd-page-header-text
    parser
    (format #f "~a, ~a."
           (string-upper-case (*act-title*))
           (string-upper-case title))
    #t)
  (add-toplevel-markup parser
    (markup #:scene (string-upper-case title)))
  (add-no-page-break parser)
  (make-music 'Music 'void #t))


%%%%%%%%%

hippolyteMark =
#(define-music-function (parser location) ()
  (make-character-mark "vhaute-contre" "Hippolyte"))

aricieMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Aricie"))

phedreMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Phèdre"))

choeurMark =
#(define-music-function (parser location) ()
  (make-character-mark "vdessus" "Chœur"))

petitChoeurMark =
#(define-music-function (parser location) ()
  (make-character-mark "vdessus" "Petit chœur"))

dianeMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Diane"))

dianeMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark "vbas-dessus"
   (markup #:character-text "Diane" text)))

theseeMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Thésée"))

theseeMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark "vbasse"
   (markup #:character-text "Thésée" text)))

tisiphoneMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Tisiphone"))

plutonMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Pluton"))

mercureMark =
#(define-music-function (parser location) ()
  (make-character-mark "vtaille" "Mercure"))

oenoneMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Œnone"))

oenoneMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark "vbas-dessus"
   (markup #:character-text "Œnone" text)))

\layout {
  \context {
    \Voice
    \override Script #'avoid-slur = #'outside
  }
}

alternativeLayout = \with {
  \consists "Horizontal_bracket_engraver"
  \override HorizontalBracket #'bracket-flare = #'(0 . 0)
  \override HorizontalBracket #'direction = #UP
}
fakeBar = {
  \once\override BreathingSign #'text = \markup \draw-line #'(0 . 4)
  \once\override BreathingSign #'Y-offset = #-2
  \breathe
}

altKeys =
#(define-music-function (parser location fractions) (list?)
   (define (make-time-sig-markup num den . rest)
     (if den
         (make-center-column-markup
          (list (number->string num)
                (number->string den)))
         (make-raise-markup -1 (number->string num))))

   (let ((time1 (apply make-time-sig-markup fractions))
         (time2 (apply make-time-sig-markup (cddr fractions))))
     #{
       \once \override Staff.TimeSignature #'stencil = #ly:text-interface::print
       \once \override Staff.TimeSignature #'text =
       \markup \override #'(baseline-skip . 0)
       \number \line { $time1 $time2 }
     #}))

#(define-markup-command (tacet layout props) ()
   (interpret-markup
    layout props
    #{ \markup TACET #}))
