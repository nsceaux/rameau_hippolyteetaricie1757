OUTPUT_DIR=out
DELIVERY_DIR=delivery
NENUVAR_LIB_PATH=$$(pwd)/../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd) -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Rameau_HippolyteEtAricie1757

# Partition complète
conducteur:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT) main.ly
.PHONY: $(PROJECT)
# Dessus
dessus:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-dessus -dpart=dessus part.ly
.PHONY: dessus
# Dessus2-Hc
dessus2-hc:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-dessus2-hc -dpart=dessus2-hc part.ly
.PHONY: dessus2-hc
# Trompette et timbales
trompette-timbales:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-trompette-timbales -dpart=trompette-timbales part-trompette-timbales.ly
.PHONY: trompette-timbales

# Cors
cor:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-cor -dpart=cor part-cor.ly
.PHONY: cor
# Parties
parties:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-parties -dpart=parties part.ly
.PHONY: parties
# Basse
basse:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-basse -dpart=basse part.ly
.PHONY: basse

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus2-hc.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus2-hc.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-trompette-timbales.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-trompette-timbales.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-cor.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-cor.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-parties.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-parties.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basse.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basse.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).html ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).html $(DELIVERY_DIR)/; fi
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: dessus dessus2-hc trompette-timbales cor parties basse
.PHONY: parts

all: check parts conducteur delivery clean
.PHONY: all

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
