\version "2.19.83"
\include "common.ily"

\paper {
  #(set! bookTitleMarkup shortBookTitleMarkup)
  ragged-bottom = ##t
}
\header { title = "Hippolyte et Aricie" }

\pieceTocNb "1-15" \markup\wordwrap {
  La prêtresse, chœur : \italic { Dieux vengeurs, lancez le tonnerre }
}
\includeScore "ADCprelude"
