%{ n°1 %}
\act "Acte Premier"
\sceneDescription\markup\column {
  \fill-line { \line { Le Théâtre représente un temple consacré à \smallCaps Diane : } }
  \fill-line { \line { On y voit un autel. } }
}
\pieceToc "Ouverture"
\includeScore "AAAouverture"
%{ n°2 %}\newBookPart #'(full-rehearsal)
\scene "Scène Première" "SCÈNE 1 : Aricie"
\sceneDescription\markup { \smallCaps Aricie en Chasseresse. }
\pieceToc\markup\wordwrap { Aricie :
  \italic { Temple sacré, séjour tranquille } }
\includeScore "AABaricie"
%%
%{ n°3 %}\newBookPart #'(full-rehearsal)
\scene "Scène II" "SCÈNE 2 : Hippolyte, Aricie"
\sceneDescription \markup \smallCaps { Hippolyte, Aricie. }
\pieceToc\markup\wordwrap { Hippolyte, Aricie :
  \italic { Princesse, quels apprêts me frappent } }
\includeScore "ABAhippolyteAricie"
%{ n°4 %}
\pieceToc\markup\wordwrap { Aricie :
  \italic { Hippolyte amoureux m'occupera sans cesse } }
\includeScore "ABBaricie"
%{ n°5 %}
\pieceToc\markup\wordwrap { Hippolyte, Aricie :
  \italic { Je vous affranchirai d'une loi si cruelle }
}
\includeScore "ABChippolyteAricie"
%{ n°6 %}\newBookPart #'(full-rehearsal)
\pieceToc\markup\wordwrap { Hippolyte, Aricie :
  \italic { Nous brûlons des plus pures flammes }
}
\includeScore "ABDduo"
%%
%{ n°7 %}\newBookPart #'(full-rehearsal)
\scene "Scène III" \markup\wordwrap {
  SCÈNE 3 : Hippolyte, Aricie, la grande prêtresse de Diane, prêtresses de Diane
}
\sceneDescription\markup\wordwrap-center \smallCaps {
  Hippolyte, Aricie, la grande prêtresse de Diane,
  prêtresses de Diane.
}
\pieceToc "Marche"
\includeScore "ACAmarche"
\partNoPageTurn#'(parties)
%{ n°8 %}\newBookPart #'(full-rehearsal)
\pieceToc\markup\wordwrap { Chœur de prêtresses :
  \italic { Dans ce paisible séjour }
}
\includeScore "ACBchoeur"
\partNoPageTurn#'(parties)
%{ n°9 %}\newBookPart #'(full-rehearsal)
\pieceToc "Premier Air"
\includeScore "ACCair"
\partPageTurn#'(parties)
%{ n°10 %}\newBookPart #'(full-rehearsal)
\pieceToc\markup\wordwrap { La grande prêtresse :
  \italic { Dieu d’Amour, pour nos asyles }
}
\includeScore "ACDpretresse"
%{ n°11 %}
\pieceToc "Deuxième Air"
\includeScore "ACEair"
%{ n°12 %}
\pieceToc\markup\wordwrap { Gavotte : \italic { De l’Amour fuyez les charmes } }
\includeScore "ACFgavotte"
%%
%{ n°13 %}\newBookPart #'(full-rehearsal)
\scene "Scène IV" \markup\wordwrap {
  SCÈNE 4 : Phèdre, Œnone, gardes, les mêmes
}
\sceneDescription\markup\smallCaps { Phèdre, Aricie, Hippolyte. }
\pieceToc\markup\wordwrap { Phèdre, Aricie, Hippolyte, chœur de prêtresses }
\includeScore "ADAphedreAricieChoeurHippolyte"
%{ n°14 %}\newBookPart #'(full-rehearsal)
\pieceToc\markup\wordwrap { Phèdre : \italic { Périsse la vaine puissance } }
\includeScore "ADBphedre"
%{ n°15 %}
\pieceToc\markup\wordwrap { La prêtresse, chœur :
  \italic { Dieux vengeurs, lancez le tonnerre }
}
\includeScore "ADCprelude"
\newBookPart #'(full-rehearsal)
\includeScore "ADDpretresseChoeur"
%{ n°16 %}\newBookPart #'(full-rehearsal)
\pieceToc "Bruit de tonnerre"
\includeScore "ADEtonnerre"
%%
%{ n°17 %}\newBookPart #'(full-rehearsal)
\scene "Scène V" \markup \wordwrap { SCÈNE 5 : Diane, les mêmes }
\sceneDescription\markup {
  \smallCaps Diane, et les acteurs de la scène précédente. }
\pieceToc\markup\wordwrap { Diane, Aricie, Hippolyte }
\includeScore "AEAdianeAricieHippolyte"
%%
%{ n°18 %}
\scene "Scène VI" "SCÈNE 6 : Phèdre"
\sceneDescription\markup\smallCaps Phèdre.
\pieceToc\markup\wordwrap { Phèdre :
  \italic { Que rien n’échappe à ma fureur } }
\includeScore "AFAphedre"
%{ n°19 %}
\pieceToc "Entr'acte"
\includeScore "AFBentracte"
\actEnd \markup { FIN DU PREMIER ACTE }
